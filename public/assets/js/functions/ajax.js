function ajax(method, headers, url, params, callback) {
    const xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP')
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4) {
            callback(xmlhttp.responseText, xmlhttp.status, url)
        }
    };
    xmlhttp.open(method, url, true);
    for (let key in headers) {
        xmlhttp.setRequestHeader(key, headers[key]);
    }
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    let body = '';
    for (let key in params) {
        body += key + '=' + params[key] + '&';
    }
    xmlhttp.send(body)
}
