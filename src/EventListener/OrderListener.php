<?php

namespace App\EventListener;

use App\Entity\Order;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderListener
{
    protected $parameterBag;
    protected $container;


    public function __construct(ParameterBagInterface $parameterBag, ContainerInterface $container)
    {
        $this->parameterBag = $parameterBag;
        $this->container = $container;
    }

    public function updateOrdersInPreparation(Order $order, LifecycleEventArgs $event)
    {
        /* @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $routeName = $request->getUri();

        $URL = $this->parameterBag->get('SYNCMODULE_URL');

        if (strpos($routeName, "loadOrders")) {
            return;
        };

        $data = [
            'id_state' => $order->getState()->getId(),
            'id_order' => $order->getIdOrder(),
        ];

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $URL . '/change_order_state.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_FOLLOWLOCATION => true
        ]);

        $output = curl_exec($ch);

        return new Response($output);

    }


}