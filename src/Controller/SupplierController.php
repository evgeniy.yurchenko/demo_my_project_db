<?php

namespace App\Controller;

use App\Entity\Supplier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class SupplierController extends AbstractController
{

    /**
     * Set cron time for supplier
     * @Route("/setCron/{id_supplier}", name="setCron")\
     * @param $id_supplier
     * @return Response
     */
    public function setCron($id_supplier)
    {
        return $this->render('admin/CronJob/cronjob.html.twig', [
            'id' => $id_supplier
        ]);
    }

    /**
     * Append cron command with custom supplier time and id
     * @Route("/setCronTime/{id_supplier}/{time}", name="setCronTime")\
     * @param $id_supplier
     * @param $time
     * @return Response
     */
    public function setCronTime($id_supplier, $time)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var Supplier $supplier */
        $supplier = $em->getRepository(Supplier::class)->find($id_supplier);
        $supplier->setCronTime($time);
        $em->persist($supplier);
        $em->flush();
        $this->writeCrons();

        return $this->redirectToRoute("prestashop_order", ["supplier" => $id_supplier]);
    }

    /**
     * Append cron command with custom supplier time and id
     * @Route("/setAutoCron/{id_supplier}/{isAutomatic}", name="setAutoCron")\
     * @param $id_supplier
     * @param $isAutomatic
     * @return Response
     */
    public function setAutoCron($id_supplier, $isAutomatic)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var Supplier $supplier */
        $supplier = $em->getRepository(Supplier::class)->find($id_supplier);
        if ($supplier) {
            $supplier->setIsAutoCron($isAutomatic);
            if (!$isAutomatic) $supplier->setCronTime(null);
            $em->persist($supplier);
        }
        $em->flush();
        $this->writeCrons();
        return $this->redirectToRoute("prestashop_order", ["supplier" => $id_supplier]);
    }

    protected function writeCrons()
    {
        $em = $this->getDoctrine()->getManager();
        /* @var Supplier[] $suppliers */
        $suppliers = $em->getRepository(Supplier::class)->findBy(["isAutoCron" => true]);
        $crons = "";
        foreach ($suppliers as $supplier) {
            $timeArray = explode(":", $supplier->getCronTime());
            if (count($timeArray) == 2) {
                $hours = $timeArray[0];
                $minutes = $timeArray[1];
                $crons .= "$minutes $hours * * * cd /home/prestaweb30dev/www/dev-erp && bin/console app:send-order " . $supplier->getId() . PHP_EOL;
            }
        }
        exec('crontab -r');
        $crons .= "0 * * * * cd /home/prestaweb30dev/www/dev-erp && bin/console app:check-orders";
        file_put_contents('/tmp/crontab.txt', $crons);
        exec('crontab /tmp/crontab.txt');
    }

}