<?php


namespace App\Controller;

use App\Entity\Order;
use App\Entity\Carrier;
use App\Entity\OrderState;
use App\Services\AddressService;
use App\Services\CarrierService;
use App\Services\CategoryService;
use App\Services\CustomerService;
use App\Services\OrderStateService;
use App\Services\ProductService;
use App\Services\SupplierService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Dompdf\Dompdf;
use Dompdf\Options;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Picqer\Barcode\Exceptions\BarcodeException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends EasyAdminController
{

    private $addressService;
    private $carrierService;
    private $categoryService;
    private $customerService;
    private $supplierService;
    private $orderStateService;
    private $productService;

    public function __construct(AddressService $addressService, CarrierService $carrierService,
                                CategoryService $categoryService, CustomerService $customerService,
                                SupplierService $supplierService, OrderStateService $orderStateService, ProductService $productService)
    {
        $this->addressService = $addressService;
        $this->carrierService = $carrierService;
        $this->categoryService = $categoryService;
        $this->customerService = $customerService;
        $this->supplierService = $supplierService;
        $this->orderStateService = $orderStateService;
        $this->productService = $productService;
    }

    public function updateAddressBatchAction(array $ids)
    {
        $this->addressService->update($ids);
    }

    public function updateProductBatchAction(array $ids)
    {
        $this->productService->update($ids);
    }

    public function updateCarrierBatchAction(array $ids)
    {
        $this->carrierService->update($ids);
    }

    public function updateCategoryBatchAction(array $ids)
    {
        $this->categoryService->update($ids);
    }

    public function updateCustomerBatchAction(array $ids)
    {
        $this->customerService->update($ids);
    }

    public function updateSupplierBatchAction(array $ids)
    {
        $this->supplierService->update($ids);
    }

    public function updateOrderStateBatchAction(array $ids)
    {
        $this->orderStateService->update($ids);
    }

    public function moveToPreparationBatchAction(array $ids)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var OrderState $state */
        $state = $em->getRepository(OrderState::class)->find(3);

        foreach ($ids as $id) {
            /* @var Order $order */
            $order = $em->getRepository(Order::class)->find($id);
            $order->setState($state);
            $em->persist($order);
        }
        $em->flush();
    }


    /**
     * Change order carrier
     *
     * @param int $orderId Order to be changed
     * @param int $carrierId New order carrier
     * @Route("/updateOrderCarrier/{orderId}/{carrierId}", name="updateOrderCarrier")
     * @return string|RedirectResponse
     */
    public function updateOrderCarrier(int $orderId, int $carrierId)
    {

        $entityManager = $this->getDoctrine()->getManager();

        /** @var Order $order */
        $order = $entityManager->getRepository(Order::class)->find($orderId);
        $order->setIdCarrier($carrierId);

        /** @var Carrier $carrier */
        $carrier = $entityManager->getRepository(Carrier::class)->find($carrierId);

        $order->setCarrier($carrier);

        $entityManager->persist($order);
        $entityManager->flush();

        return new Response('Carrier changed');
    }

    /**
     * Change order weight
     *
     * @param int $orderId Order to be changed
     * @param string $weight New order weight
     * @Route("/updateOrderWeight/{orderId}/{weight}", name="updateOrderWeight")
     * @return string|RedirectResponse
     */
    public function updateOrderWeight(int $orderId, string $weight)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $order = $entityManager->getRepository(Order::class)->find($orderId);
        $weight = floatval($weight);
        $order->setOrderWeight($weight);

        $entityManager->persist($order);
        $entityManager->flush();

        return new Response('Weight changed');
    }

    /**
     * Get all Carriers
     *
     * @Route("/getCarries", name="get_carriers")
     * @return string|RedirectResponse
     */
    public function getCarries()
    {

        $entityManager = $this->getDoctrine()->getManager();

        /* @var Carrier[] $carries */
        $carries = $entityManager->getRepository(Carrier::class)->findAll();
        $carriesArray = [];
        foreach ($carries as $carrier) {
            array_push($carriesArray, [
                "id" => $carrier->getId(),
                "name" => $carrier->getName()
            ]);
        }

        return new Response(json_encode($carriesArray));
    }


    /**
     * Get all Carriers
     *
     * @Route("/getStates", name="get_states")
     * @return Response
     */
    public function getStates()
    {
        $entityManager = $this->getDoctrine()->getManager();

        /* @var OrderState[] $states */
        $states = $entityManager->getRepository(OrderState::class)->findAll();
        $statesArray = [];
        foreach ($states as $state) {
            array_push($statesArray, [
                'id' => $state->getId(),
                'name' => $state->getName(),
                'color' => $state->getColor()
            ]);
        }

        return new Response(json_encode($statesArray));
    }

    /**
     * Change order state
     *
     * @Route("/updateOrderState/{order}/{state}")
     * @param Order $order
     * @param OrderState $state
     * @return Response
     */
    public function updateOrderState(Order $order, OrderState $state)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $state->addOrder($order);
        $entityManager->persist($state);

        $entityManager->flush();

        return new Response($state->getColor());
    }

    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null)
    {
        if ($this->entity['class'] !== Order::class) return parent::createListQueryBuilder($entityClass, $sortDirection, $sortField, $dqlFilter);
        /* @var EntityManager */
        $em = $this->getDoctrine()->getManagerForClass($this->entity['class']);
        /* @var QueryBuilder */
        $queryBuilder = $em->createQueryBuilder()
            ->select('entity')
            ->from($this->entity['class'], 'entity')
            ->leftJoin('entity.state', 'orderState');

        if (!empty($dqlFilter)) {
            $queryBuilder->andWhere($dqlFilter);
        }

        if (null !== $sortField) {
            $queryBuilder->orderBy('entity.' . $sortField, $sortDirection ?: 'DESC');
        }

        return $queryBuilder;
    }

}