<?php

namespace App\Controller;

use App\Entity\PrestashopOrder;
use App\Services\AddressService;
use App\Services\PrestashopService;
use App\Services\ProcessPdf;
use Dompdf\Dompdf;
use Dompdf\Options;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

class PrintPdfController extends AbstractController
{

    protected $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;

    }


    /**
     * @Route("/procPdf", name="procPdf")
     * @param Request $request
     * @return Response
     */
    public function procPdf(Request $request)
    {
        $parser_dir = $this->parameterBag->get('kernel.project_dir') . "/" . $this->parameterBag->get('PDF_PARSE_DIR');
        $parser_url = $this->parameterBag->get('PDF_PARSE_URL');
        $parser_login = $this->parameterBag->get('PDF_PARSE_LOGIN');
        $parser_password = $this->parameterBag->get('PDF_PARSE_PASSWORD');

        $download_directory = $parser_dir . "download/";
        $config_file = $parser_dir . "config.json";
        $config_json = json_decode(file_get_contents($config_file), true);
        $config_json["download_path"] = $download_directory;
        $config_json["target_url"] = $parser_url;
        $config_json["credentials"]["login"] = $parser_login;
        $config_json["credentials"]["password"] = $parser_password;
        file_put_contents($config_file, json_encode($config_json));
        $process = new Process(['node', $parser_dir . 'app.js']);
        $process->setTimeout(null);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        });

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output = $process->getOutput();
        $output_array = explode("---filename---", $output);
        if (!isset($output_array[1])) {
            $referer = $request->headers->get('referer');
            $this->addFlash("warning", "smth went wrong");
            return $this->redirect($referer);
        }
        $file = $output_array[1];
        $fileContent = file_get_contents($download_directory . $file);
        unlink($download_directory . $file);

        $response = new Response($fileContent);
        $response->headers->set('Content-Type', 'application/pdf');
        /*$disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $file
        );

        $response->headers->set('Content-Disposition', $disposition);*/
        return $response;
    }
}
