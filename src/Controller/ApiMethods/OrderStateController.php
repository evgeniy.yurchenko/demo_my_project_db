<?php

namespace App\Controller\ApiMethods;

use App\Services\OrderStateService;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderStateController extends AbstractController
{
    /**
     * Update webservice "order_states" from local database
     *
     * @param $ids
     * @param Request $request
     * @param OrderStateService $orderStateService
     * @return RedirectResponse
     * @throws PrestaShopWebserviceException
     * @Route("/updateOrderState/{ids}", name="updateOrderState")
     */
    public function update($ids, Request $request, OrderStateService $orderStateService)
    {
        $ids = json_decode($ids);
        $prevLink = urldecode($request->get('prevLink'));

        $orderStateService->update($ids);

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

    /**
     * Load data from webservice "order_states" to local database
     *
     * @param Request $request
     * @param OrderStateService $orderStateService
     * @return string|RedirectResponse
     * @Route("/loadOrderStates", name="loadOrderState")
     */
    public function load(Request $request, OrderStateService $orderStateService)
    {
        $prevLink = $request->headers->get('referer');

        $orderStateService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Update all fields on webservice "order_states" from local database
     *
     * @param Request $request
     * @param OrderStateService $orderStateService
     * @return RedirectResponse
     * @Route("/updateAllOrderStates", name="updateAllOrderStates")
     */
    public function updateAll(Request $request, OrderStateService $orderStateService)
    {
        $prevLink = $request->headers->get('referer');

        $orderStateService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}