<?php

namespace App\Controller\ApiMethods;

use App\Entity\Stock;
use App\Services\StockService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StockController extends AbstractController
{
    /**
     * Load data from webservice "stocks" to local database
     *
     * @param Request $request
     * @param StockService $stockService
     * @return string|RedirectResponse
     * @Route("/loadStocks", name="loadStocks")
     */
    public function load(Request $request, StockService $stockService)
    {
        $prevLink = $request->headers->get('referer');

        $stockService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

    /**
     * Update all fields on webservice "orders" from local database
     *
     * @param Request $request
     * @param StockService $stockService
     * @return RedirectResponse
     * @Route("/updateAllStocks", name="updateAllStocks")
     */
    public function updateAll(Request $request, StockService $stockService)
    {
        $prevLink = $request->headers->get('referer');

        $stockService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }


}