<?php

namespace App\Controller\ApiMethods;

use App\Services\ProductService;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * Load data from webservice "products" to local database
     *
     * @param Request $request
     * @param ProductService $productService
     * @return string|RedirectResponse
     * @throws PrestaShopWebserviceException
     * @Route("/loadProducts", name="loadProducts")
     */
    public function load(Request $request, ProductService $productService)
    {
        $prevLink = $request->headers->get('referer');

        $productService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}