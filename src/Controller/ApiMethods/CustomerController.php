<?php

namespace App\Controller\ApiMethods;

use App\Services\CustomerService;
use Exception;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CustomerController extends AbstractController
{
    /**
     * Update webservice "customers" from local database
     *
     * @param $ids
     * @param Request $request
     * @param CustomerService $customerService
     * @return RedirectResponse
     * @throws PrestaShopWebserviceException
     * @Route("/updateCustomers/{ids}", name="updateCustomers")
     */
    public function update($ids, Request $request, CustomerService $customerService)
    {
        $ids = json_decode($ids);
        $prevLink = urldecode($request->get('prevLink'));

        $customerService->update($ids);

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

    /**
     * Load data from webservice "customers" to local database
     *
     * @param Request $request
     * @param CustomerService $customerService
     * @return string|RedirectResponse
     * @throws Exception
     * @Route("/loadCustomers", name="loadCustomers")
     */
    public function load(Request $request, CustomerService $customerService)
    {
        $prevLink = $request->headers->get('referer');

        $customerService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

    /**
     * Update all fields on webservice "customers" from local database
     *
     * @param Request $request
     * @param CustomerService $customerService
     * @return RedirectResponse
     * @Route("/updateAllCustomers", name="updateAllCustomers")
     */
    public function updateAll(Request $request, CustomerService $customerService)
    {
        $prevLink = $request->headers->get('referer');

        $customerService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}