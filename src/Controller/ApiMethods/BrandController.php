<?php

namespace App\Controller\ApiMethods;

use App\Services\BrandService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BrandController extends AbstractController
{

    /**
     * @param Request $request
     * @param BrandService $brandService
     * @return RedirectResponse
     * @throws Exception
     * @Route("/loadBrands", name="loadBrands")
     */
    public function load(Request $request, BrandService $brandService)
    {
        $prevLink = $request->headers->get('referer');

        $brandService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * @param Request $request
     * @param BrandService $brandService
     * @return RedirectResponse
     * @throws Exception
     * @Route("/updateAllBrands", name="updateAllBrands")
     */
    public function updateAll(Request $request, BrandService $brandService)
    {
        $prevLink = $request->headers->get('referer');

        $brandService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}