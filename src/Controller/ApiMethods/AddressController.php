<?php

namespace App\Controller\ApiMethods;

use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Services\AddressService;
use Symfony\Component\Routing\Annotation\Route;

class AddressController extends AbstractController
{
    /**
     * Update selected webservice "address" from local database
     *
     * @param $ids
     * @param Request $request
     * @param AddressService $addressService
     * @return RedirectResponse
     * @throws PrestaShopWebserviceException
     * @Route("/updateAddresses/{ids}", name="updateAddresses")
     */
    public function update($ids, Request $request, AddressService $addressService)
    {
        $ids = json_decode($ids);
        $prevLink = urldecode($request->get('prevLink'));

        $addressService->update($ids);

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Load data from webservice "address" to local database
     *
     * @param Request $request
     * @param AddressService $addressService
     * @return string|RedirectResponse
     * @Route("/loadAddresses", name="loadAddresses")
     */
    public function load(Request $request, AddressService $addressService)
    {
        $prevLink = $request->headers->get('referer');
        $addressService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Update all fields on webservice "address" from local database
     *
     * @param Request $request
     * @param AddressService $addressService
     * @return RedirectResponse
     * @Route("/updateAllAddresses", name="updateAllAddresses")
     */
    public function updateAll(Request $request, AddressService $addressService)
    {
        $prevLink = $request->headers->get('referer');
        $addressService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}