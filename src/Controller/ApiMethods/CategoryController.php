<?php

namespace App\Controller\ApiMethods;

use App\Services\CategoryService;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

// TODO: Need to fix.
class CategoryController extends AbstractController
{
    /**
     * Update webservice "categories" from local database
     *
     * @param $ids
     * @param Request $request
     * @param CategoryService $categoryService
     * @return RedirectResponse
     * @throws PrestaShopWebserviceException
     * @Route("/updateCategories/{ids}", name="updateCategories")
     */
    public function update($ids, Request $request, CategoryService $categoryService)
    {
        $ids = json_decode($ids);
        $prevLink = urldecode($request->get('prevLink'));

        $categoryService->update($ids);

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Load data from webservice "categories" to local database
     *
     * @param Request $request
     * @param CategoryService $categoryService
     * @return string|RedirectResponse
     * @Route("/loadCategories", name="loadCategories")
     */
    public function load(Request $request, CategoryService $categoryService)
    {
        $prevLink = $request->headers->get('referer');

        $categoryService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

    /**
     * Update all fields on webservice "categories" from local database
     *
     * @param Request $request
     * @param CategoryService $categoryService
     * @return RedirectResponse
     * @Route("/updateAllCategories", name="updateAllCategories")
     */
    public function updateAll(Request $request, CategoryService $categoryService)
    {
        $prevLink = $request->headers->get('referer');

        $categoryService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}