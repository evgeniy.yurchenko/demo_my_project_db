<?php

namespace App\Controller\ApiMethods;

use App\Services\ProductAttributesService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductAttributeController extends AbstractController
{
    /**
     * Load data from webservice "product_attributes" to local database
     *
     * @param Request $request
     * @param ProductAttributesService $productAttributesService
     * @return string|RedirectResponse
     * @throws Exception
     * @Route("/loadProductAttributes", name="loadProductAttributes")
     */
    public function load(Request $request, ProductAttributesService $productAttributesService)
    {
        $prevLink = $request->headers->get('referer');

        $productAttributesService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Update all fields on webservice "product_attribute" from local database
     *
     * @param Request $request
     * @param ProductAttributesService $productAttributesService
     * @return RedirectResponse
     * @Route("/updateAllProductAttributes", name="updateAllProductAttributes")
     */
    public function updateAll(Request $request, ProductAttributesService $productAttributesService)
    {
        $prevLink = $request->headers->get('referer');

        $productAttributesService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}