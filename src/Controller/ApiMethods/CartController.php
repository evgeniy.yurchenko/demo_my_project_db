<?php

namespace App\Controller\ApiMethods;

use App\Services\CartService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * Load data from webservice "carts" to local database
     *
     * @param Request $request
     * @param CartService $cartService
     * @return string|RedirectResponse
     * @throws Exception
     * @Route("/loadCarts", name="loadCarts")
     */
    public function load(Request $request, CartService $cartService)
    {
        $prevLink = $request->headers->get('referer');

        $cartService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }


    // TODO: Need to final

    /**
     * Update all fields on webservice "carts" from local database
     *
     * @param Request $request
     * @param CartService $cartService
     * @return RedirectResponse
     * @Route("/updateAllCarts", name="updateAllCarts")
     */
    public function updateAll(Request $request, CartService $cartService)
    {
        $prevLink = $request->headers->get('referer');

        $cartService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

}