<?php

namespace App\Controller\ApiMethods;

use App\Services\CarrierService;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarrierController extends AbstractController
{
    /**
     * Update selected webservice "carrier" from local database
     *
     * @param $ids
     * @param Request $request
     * @param CarrierService $carrierService
     * @return RedirectResponse
     * @throws PrestaShopWebserviceException
     * @Route("/updateCarriers/{ids}", name="updateCarriers")
     */
    public function update($ids, Request $request, CarrierService $carrierService)
    {
        $ids = json_decode($ids);
        $prevLink = urldecode($request->get('prevLink'));

        $carrierService->update($ids);

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Load data from webservice "carrier" to local database
     *
     * @param Request $request
     * @param CarrierService $carrierService
     * @return string|RedirectResponse
     * @Route("/loadCarriers", name="loadCarriers")
     */
    public function load(Request $request, CarrierService $carrierService)
    {
        $prevLink = $request->headers->get('referer');

        $carrierService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

    /**
     * Update all fields on webservice "carrier" from local database
     *
     * @param Request $request
     * @param CarrierService $carrierService
     * @return RedirectResponse
     * @Route("/updateAllCarriers", name="updateAllCarriers")
     */
    public function updateAll(Request $request, CarrierService $carrierService)
    {
        $prevLink = $request->headers->get('referer');

        $carrierService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}