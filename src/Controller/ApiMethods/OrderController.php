<?php

namespace App\Controller\ApiMethods;

use App\Entity\Order;
use App\Entity\OrderState;
use App\Services\OrderService;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * Load data from webservice "orders" to local database
     *
     * @param Request $request
     * @param OrderService $orderService
     * @return string|RedirectResponse
     * @Route("/loadOrders", name="loadOrders")
     */
    public function load(Request $request, OrderService $orderService)
    {
        $prevLink = $request->headers->get('referer');

        $orderService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);

    }

    /**
     * Update all fields on webservice "orders" from local database
     *
     * @param Request $request
     * @param OrderService $orderService
     * @return RedirectResponse
     * @Route("/updateAllOrders", name="updateAllOrders")
     */
    public function updateAll(Request $request, OrderService $orderService)
    {
        $prevLink = $request->headers->get('referer');

        $orderService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}