<?php

namespace App\Controller\ApiMethods;

use App\Services\OrderDetailsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderDetailsController extends AbstractController
{
    /**
     * Load data from webservice "order_details" to local database
     *
     * @param Request $request
     * @param OrderDetailsService $orderDetailsService
     * @return string|RedirectResponse
     * @Route("/loadOrderDetails", name="loadOrderDetails")
     */
    public function load(Request $request, OrderDetailsService $orderDetailsService)
    {
        $prevLink = $request->headers->get('referer');

        $orderDetailsService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

}