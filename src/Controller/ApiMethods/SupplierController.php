<?php

namespace App\Controller\ApiMethods;

use App\Services\SupplierService;
use PrestaShopWebserviceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SupplierController extends AbstractController
{
    /**
     * Update webservice "suppliers" from local database
     *
     * @param $ids
     * @param Request $request
     * @param SupplierService $supplierService
     * @return RedirectResponse
     * @throws PrestaShopWebserviceException
     * @Route("/updateSuplier/{ids}", name="updateSuppliers")
     */
    public function update($ids, Request $request, SupplierService $supplierService)
    {
        $ids = json_decode($ids);
        $prevLink = urldecode($request->get('prevLink'));

        $supplierService->update($ids);

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Load data from webservice "suppliers" to local database
     *
     * @param Request $request
     * @param SupplierService $supplierService
     * @return string|RedirectResponse
     * @Route("/loadSuppliers", name="loadSuppliers")
     */
    public function load(Request $request, SupplierService $supplierService)
    {
        $prevLink = $request->headers->get('referer');

        $supplierService->load();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }

    /**
     * Update all fields on webservice "suppliers" from local database
     *
     * @param Request $request
     * @param SupplierService $supplierService
     * @return RedirectResponse
     * @Route("/updateAllSuppliers", name="updateAllSuppliers")
     */
    public function updateAll(Request $request, SupplierService $supplierService)
    {
        $prevLink = $request->headers->get('referer');

        $supplierService->updateAll();

        if ($prevLink === null) {
            return $this->redirect('/');
        }

        return $this->redirect($prevLink);
    }
}