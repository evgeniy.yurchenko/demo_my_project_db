<?php

namespace App\Controller\ApiMethods;

use App\Entity\SupplierOrder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SupplierOrdersController extends AbstractController
{
    /**
     * @return Response
     * @Route("/supplier/orders", name="supplier_orders")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $supplierOrders = $entityManager->getRepository(SupplierOrder::class)
            ->findAll();

        return $this->render('admin/ezadmin_additions/supplier_orders/index.html.twig', [
            'supplier_orders' => $supplierOrders
        ]);
    }
}
