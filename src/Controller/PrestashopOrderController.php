<?php

namespace App\Controller;

use App\Entity\CompanyAddress;
use App\Entity\OfflineProduct;
use App\Entity\OrderProduct;
use App\Entity\PrestashopOrder;
use App\Entity\PrestashopOrderDetail;
use App\Entity\Product;
use App\Entity\Stock;
use App\Entity\Supplier;
use App\Form\OfflineProductType;
use Exception;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class PrestashopOrderController extends AbstractController
{
    protected $parameterBag;
    protected $mailer;


    public function __construct(ParameterBagInterface $parameterBag, Swift_Mailer $mailer)
    {
        $this->parameterBag = $parameterBag;
        $this->mailer = $mailer;
    }

    /**
     * @param Supplier|null $supplier
     * @param Request $request
     * @return Response
     * @Route("/admin/prestashop/order/{supplier?}", name="prestashop_order")
     */
    public function index(?Supplier $supplier, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /* @var Supplier[] $suppliers */
        $suppliers = $entityManager->getRepository(Supplier::class)->findAll();

        if ($supplier) {
            $orders = $supplier->getPrestashopOrders();
        } else {
            $orders = $entityManager->getRepository(PrestashopOrder::class)->findAll();
        }

        $companyAddresses = $entityManager->getRepository(CompanyAddress::class)->findAll();
        $form = $this->createForm(OfflineProductType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $offlineProduct = $form->getData();
            $order = $form->get('order')->getData();
            $supplier = $form->get('supplier')->getData();
            $supplier = $entityManager->getRepository(Supplier::class)->find($supplier);
            $order = $entityManager->getRepository(PrestashopOrder::class)->find($order);
            $offlineProduct->setSupplier($supplier);
            $entityManager->persist($offlineProduct);
            $prestashopOrderDetail = new PrestashopOrderDetail();
            $prestashopOrderDetail->setOfflineProduct($offlineProduct);
            $prestashopOrderDetail->setPrestashopOrder($order);
            $prestashopOrderDetail->setPrice(0);
            $prestashopOrderDetail->setQuantity(0);
            $entityManager->persist($prestashopOrderDetail);
            $entityManager->flush();

        }

        return $this->render('admin/ezadmin_additions/prestashop_order/index.html.twig', [
            'orders' => $orders,
            'suppliers' => $suppliers,
            'currentIdSupplier' => $supplier ? $supplier->getId() : null,
            'isAutoCron' => $supplier ? $supplier->getIsAutoCron() : false,
            'currentCronTime' => $supplier ? $supplier->getCronTime() : null,
            'company_addresses' => $companyAddresses,
            'offlineProductForm' => $form->createView()
        ]);
    }

    /**
     * @param $id
     * @param $quantity_validated
     * @param Request $request
     * @return Response
     * @Route("/prestashop_order/update_quantity_validated/{id}/{quantity_validated}", name="prestashop_orderUpdate_quantity_validated")
     */
    public function update_quantity_validated($id, $quantity_validated, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /* @var PrestashopOrderDetail $orderDetail */
        $orderDetail = $entityManager->getRepository(PrestashopOrderDetail::class)->find($id);
        //product id, product_attr
        $orderDetail->setQuantityValidated($quantity_validated);
        $entityManager->persist($orderDetail);

        if ($orderDetail->getProduct()) {
            /** @var Stock $stock */
            $stock = $orderDetail->getProduct()->getStock($orderDetail->getProductAttribute());

            $stockQuantity = $stock->getQuantity();

            $stockQuantity += $quantity_validated;
            $stock->setQuantity($stockQuantity);

            $entityManager->persist($stock);
        }

        if ($orderDetail->getOfflineProduct()) {
            $stockQuantity = $orderDetail->getOfflineProduct()->getQuantity();
            $orderDetail->getOfflineProduct()->setQuantity($stockQuantity + $quantity_validated);
        }

        $entityManager->flush();

        $URL = $this->parameterBag->get('SYNCMODULE_URL');

        $id_product = $orderDetail->getProduct()->getIdProduct();
        $id_product_attribute = $orderDetail->getProductAttribute() ? $orderDetail->getProductAttribute()->getIdProductAttribute() : 0;

        $data = [
            'stock' => $quantity_validated,
            'id_product' => $id_product,
            'id_product_attribute' => $id_product_attribute
        ];


        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $URL . '/add_product_stock.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_FOLLOWLOCATION => true
        ]);

        $output = curl_exec($ch);

        return new Response($output);
    }

    /**
     * @param $id
     * @param $quantity_ordered
     * @param Request $request
     * @return Response
     * @Route("/prestashop_order/update_quantity_ordered/{id}/{quantity_ordered}", name="prestashop_orderUpdate_quantity_ordered")
     */
    public function update_quantity_ordered($id, $quantity_ordered, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $orderDetail = $entityManager->getRepository(PrestashopOrderDetail::class)->find($id);
        $orderDetail->setQuantity($quantity_ordered);

        if ($orderDetail->getProductAttribute())
            $orderDetail->setPrice($orderDetail->getProductAttribute()->getWholesalePrice() * $quantity_ordered);
        if ($orderDetail->getProduct())
            $orderDetail->setPrice($orderDetail->getProduct()->getWholesalePrice() * $quantity_ordered);
        if ($orderDetail->getOfflineProduct())
            $orderDetail->setPrice($orderDetail->getOfflineProduct()->getWholesalePrice() * $quantity_ordered);


        $entityManager->flush();

        return new Response("Success");
    }

    /**
     * @param PrestashopOrder $order
     * @return false|string
     * @Route("/prestashop_order/validate_order/{order}", name="prestashop_order.validate_order")
     */
    public function validate_order(PrestashopOrder $order)
    {
        $order->setStatus(PrestashopOrder::STATUS_VALIDATED);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($order);
        $entityManager->flush();

        return new Response('Success');
    }

    /**
     * @param $currentIdSupplier
     * @param KernelInterface $kernel
     * @return Response
     * @throws Exception
     * @Route("/prestashop_order/load_today_orders/{currentIdSupplier}", name="prestashop_orderLoad_today_orders")
     */
    public function load_today_orders($currentIdSupplier, KernelInterface $kernel)
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'app:send-orders',
            'supplier' => $currentIdSupplier,
        ]);

        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();

        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();
        // return new Response(""), if you used NullOutput()
        return $this->redirectToRoute("prestashop_order", ["supplier" => $currentIdSupplier]);
    }

    /**
     * @param $orderId
     * @param Request $request
     * @return Response
     * @Route("/prestashop_order/set_order_address/{orderId}", name="prestashop_orderSetOrderAddress")
     */
    public function set_order_address($orderId, Request $request)
    {
        $address = $request->query->get('address');
        $entityManager = $this->getDoctrine()->getManager();

        $company_address = $entityManager->getRepository(CompanyAddress::class)
            ->find($address);

        $order = $entityManager->getRepository(PrestashopOrder::class)
            ->find($orderId);

        $order->setCompanyAddress($company_address);

        $entityManager->flush();

        return new Response('Success');
    }

    /**
     * @param $orderId
     * @param Request $request
     * @return Response
     * @Route("/prestashop_order/set_order_invoice_address/{orderId}", name="prestashop_orderSetOrderInvoiceAddress")
     */
    public function set_order_invoice_address($orderId, Request $request)
    {
        $address = $request->query->get('address');
        $entityManager = $this->getDoctrine()->getManager();

        $company_address = $entityManager->getRepository(CompanyAddress::class)
            ->find($address);

        $order = $entityManager->getRepository(PrestashopOrder::class)
            ->find($orderId);

        $order->setInvoiceAddress($company_address);

        $entityManager->flush();

        return new Response('Success');
    }

    /**
     * @param $orderProductId
     * @param $isValidate
     * @return Response
     * @Route("/prestashop_order/change_product_validate/{orderProductId}/{isValidate}", name="prestashop_orderChangeProductValidate")
     */
    public function changeOrderProductValidated($orderProductId, $isValidate)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var OrderProduct $orderProduct */
        $orderProduct = $entityManager->getRepository(OrderProduct::class)
            ->find($orderProductId);

        $orderProductIsValidated = !$orderProduct->getIsValidated();

        $orderProduct->setIsValidated($orderProductIsValidated);

        $entityManager->flush();

        return new Response($orderProductIsValidated);
    }

    /**
     * @param $orderProductId
     * @param $value
     * @param $ent_class
     * @Route("/prestashop_order/change_product_wholesale_price/{orderProductId}/{value}/{ent_class}", name="prestashop_orderChangeProductWholesalePrice")
     * @return Response
     */
    public function changeOrderProductWholesalePrice($orderProductId, $value, $ent_class)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $class = $ent_class == "product" ? Product::class : OfflineProduct::class;
        $orderProduct = $entityManager->getRepository($class)
            ->find($orderProductId);


        $orderProduct->setWholesalePrice($value);

        $entityManager->persist($orderProduct);
        $entityManager->flush();


        return new Response('Success');
    }

    /**
     * @param $id
     * @param $quantity
     * @return Response
     * @Route("/prestashop_order/change_quantity/{id}/{quantity}", name="prestashop_orderChangeQuantity")
     */
    public function changeOrderedQuantity($id, $quantity)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var PrestashopOrderDetail $psOrderDetail */
        $psOrderDetail = $entityManager->getRepository(PrestashopOrderDetail::class)->find($id);

        $psOrderDetail->setQuantity($quantity);

        $entityManager->persist($psOrderDetail);
        $entityManager->flush();

        return new Response('Success');
    }

    /**
     * @param $id
     * @param $quantity
     * @return Response
     * @Route("/prestashop_order/change_product_quantity/{id}/{quantity}", name="prestashop_orderChangeProductQuantity")
     */
    public function changeProductQuantity($id, $quantity)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $entityManager->getRepository(Product::class)->find($id);

        $product->setQuantity($quantity);

        $entityManager->persist($product);
        $entityManager->flush();

        return new Response('Success');
    }

    /**
     * @param $supplier
     * @param Request $request
     * @return Response
     * @Route("/prestashop_order/search_products/{supplier}", name="prestashop_orderSearchProducts")
     */
    public function searchProducts($supplier, Request $request)
    {
        $query = $request->get('query');
        $products = $this->getDoctrine()->getManager()->getRepository(Product::class)
            ->searchByNameStart($supplier, $query, 30);

        return $this->render('admin/components/products/table.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @param $supplier
     * @param Request $request
     * @return Response
     * @Route("/prestashop_order/search_offline_products/{supplier}", name="prestashop_orderSearchOfflineProducts")
     */
    public function searchOfflineProducts($supplier, Request $request)
    {
        $query = $request->get('query');
        $products = $this->getDoctrine()->getManager()->getRepository(OfflineProduct::class)->searchByNameStart($supplier, $query, 30);

        return $this->render('admin/components/products/table.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @param PrestashopOrder $order
     * @param Product $product
     * @return Response
     * @Route("/prestashop_order/addProductToOrderDetails/{order}/{product}", name="prestashop_orderAddProductToOrderDetails")
     */
    public function addProductToOrderDetails(PrestashopOrder $order, Product $product)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $orderDetails = new PrestashopOrderDetail();
        $orderDetails->setProduct($product);
        $orderDetails->setPrestashopOrder($order);
        $orderDetails->setQuantity(0);
        $orderDetails->setPrice(0);

        $entityManager->persist($orderDetails);
        $entityManager->flush();

        return $this->render('admin/components/PrestashopOrder/product_item.html.twig', [
            'orderDetails' => $orderDetails,
        ]);
    }

    /**
     * @param PrestashopOrder $order
     * @param OfflineProduct $product
     * @return Response
     * @Route("/prestashop_order/addOfflineProductToOrderDetails/{order}/{product}", name="prestashop_orderAddOfflineProductToOrderDetails")
     */
    public function addOfflineProductToOrderDetails(PrestashopOrder $order, OfflineProduct $product)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $orderDetails = new PrestashopOrderDetail();
        $orderDetails->setOfflineProduct($product);
        $orderDetails->setPrestashopOrder($order);
        $orderDetails->setQuantity(0);
        $orderDetails->setPrice(0);

        $entityManager->persist($orderDetails);
        $entityManager->flush();

        return $this->render('admin/components/PrestashopOrder/product_item.html.twig', [
            'orderDetails' => $orderDetails,
        ]);
    }
}
