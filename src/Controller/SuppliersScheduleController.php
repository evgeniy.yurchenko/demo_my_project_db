<?php

namespace App\Controller;

use App\Entity\Supplier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SuppliersScheduleController extends AbstractController
{
    /**
     * @param $id
     * @return Response
     * @Route("/suppliers/schedule/{id?}", name="suppliers_schedule")
     */
    public function index($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var Supplier $suppliers */
        $suppliers = $entityManager->getRepository(Supplier::class)->findAll();

        if ($id != null) {
            /** @var Supplier $supplier */
            $supplier = $entityManager->getRepository(Supplier::class)->find($id);

            $getSchedule = $supplier->getSchedule();

            $scheduleData = [
                'mon' => explode('-', $getSchedule['mon']),
                'tue' => explode('-', $getSchedule['tue']),
                'wed' => explode('-', $getSchedule['wed']),
                'thu' => explode('-', $getSchedule['thu']),
                'fri' => explode('-', $getSchedule['fri']),
                'sat' => explode('-', $getSchedule['sat']),
                'sun' => explode('-', $getSchedule['sun']),
            ];

            return $this->render('admin/ezadmin_additions/suppliers_schedule/index.html.twig', [
                'suppliers' => $suppliers,
                'currentIdSupplier' => $id,
                'supplier' => $supplier,
                'scheduleData' => $scheduleData
            ]);
        } else {
            return $this->render('admin/ezadmin_additions/suppliers_schedule/index.html.twig', [
                'suppliers' => $suppliers,
                'currentIdSupplier' => $id,
                'scheduleData' => null
            ]);
        }

    }

    /**
     * @Route("/suppliers/updateSupplierSchedule/{id}")
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     * @Route("/suppliers/updateSuppliersSchedule/{id}", name="updateSuppliersSchedule")
     */
    public function updateSuppliersSchedule($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var Supplier $supplier */
        $supplier = $entityManager->getRepository(Supplier::class)->find($id);

        $getValue = $request->request;

        if (!empty($getValue->get('monday_from')) && !empty($getValue->get('monday_to'))) {
            $monday = $getValue->get('monday_from') . ' - ' . $getValue->get('monday_to');
        } else {
            $monday = null;
        }

        if (!empty($getValue->get('tuesday_from')) && !empty($getValue->get('tuesday_to'))) {
            $tuesday = $getValue->get('tuesday_from') . ' - ' . $getValue->get('tuesday_to');
        } else {
            $tuesday = null;
        }

        if (!empty($getValue->get('wednesday_from')) && !empty($getValue->get('wednesday_to'))) {
            $wednesday = $getValue->get('wednesday_from') . ' - ' . $getValue->get('wednesday_to');
        } else {
            $wednesday = null;
        }

        if (!empty($getValue->get('thursday_from')) && !empty($getValue->get('thursday_to'))) {
            $thursday = $getValue->get('thursday_from') . ' - ' . $getValue->get('thursday_to');
        } else {
            $thursday = null;
        }

        if (!empty($getValue->get('friday_from')) && !empty($getValue->get('friday_to'))) {
            $friday = $getValue->get('friday_from') . ' - ' . $getValue->get('friday_to');
        } else {
            $friday = null;
        }

        if (!empty($getValue->get('saturday_from')) && !empty($getValue->get('saturday_to'))) {
            $saturday = $getValue->get('saturday_from') . ' - ' . $getValue->get('saturday_to');
        } else {
            $saturday = null;
        }

        if (!empty($getValue->get('sunday_from')) && !empty($getValue->get('sunday_to'))) {
            $sunday = $getValue->get('sunday_from') . ' - ' . $getValue->get('sunday_to');
        } else {
            $sunday = null;
        }

        $supplier->setSchedule([
            'mon' => $monday,
            'tue' => $tuesday,
            'wed' => $wednesday,
            'thu' => $thursday,
            'fri' => $friday,
            'sat' => $saturday,
            'sun' => $sunday
        ]);

        $entityManager->persist($supplier);
        $entityManager->flush();

        return $this->redirectToRoute('suppliers_schedule', [
            'id' => $id
        ]);
    }

}
