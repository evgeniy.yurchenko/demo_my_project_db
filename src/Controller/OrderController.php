<?php

namespace App\Controller;

use App\Entity\OrderState;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OrderController extends AbstractController
{

    /**
     * @Route("/getAllStates/{orderId}/{currentStateId}")
     * @param $orderId
     * @param $currentStateId
     * @return string
     */
    public function getAllStates($orderId, $currentStateId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $orderStates = $entityManager->getRepository(OrderState::class)->findAll();

        return $this->render('admin/components/Order/changeOrderStateList.html.twig', [
            'states' => $orderStates,
            'currentState' => $currentStateId,
            'order' => $orderId
        ]);
    }

}