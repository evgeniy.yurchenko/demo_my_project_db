<?php

namespace App\Controller;

use App\Services\AddressService;
use App\Services\BrandService;
use App\Services\CarrierService;
use App\Services\CartService;
use App\Services\CategoryService;
use App\Services\CustomerService;
use App\Services\OrderDetailsService;
use App\Services\OrderService;
use App\Services\OrderStateService;
use App\Services\ProductAttributesService;
use App\Services\ProductService;
use App\Services\StockService;
use App\Services\SupplierService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LoadAllController extends AbstractController
{
    protected $addressService, $carrierService, $cartService,
        $categoryService, $customerService, $orderDetailService,
        $orderService, $orderStateService, $productService, $productAttributesService,
        $stockService, $supplierService, $brandService;

    public function __construct(AddressService $addressService, CarrierService $carrierService,
                                CartService $cartService, CategoryService $categoryService,
                                CustomerService $customerService, OrderDetailsService $orderDetailsService,
                                OrderService $orderService, OrderStateService $orderStateService,
                                ProductAttributesService $productAttributesService, ProductService $productService,
                                StockService $stockService, SupplierService $supplierService,
                                BrandService $brandService)
    {
        $this->addressService = $addressService;
        $this->carrierService = $carrierService;
        $this->cartService = $cartService;
        $this->categoryService = $categoryService;
        $this->customerService = $customerService;
        $this->orderDetailService = $orderDetailsService;
        $this->orderService = $orderService;
        $this->orderStateService = $orderStateService;
        $this->productService = $productService;
        $this->productAttributesService = $productAttributesService;
        $this->stockService = $stockService;
        $this->supplierService = $supplierService;
        $this->brandService = $brandService;
    }

    /**
     * @Route("/loadAll", name="load_all")
     * @throws Exception
     */
    public function index()
    {
        $this->supplierService->load();
        sleep(1);
        $this->carrierService->load();
        sleep(1);
        $this->productService->load();
        $this->productAttributesService->load();
        sleep(1);
        $this->categoryService->load();
        sleep(1);
        $this->customerService->load();
        $this->addressService->load();
        $this->orderStateService->load();
        $this->cartService->load();
        $this->orderService->load();
        $this->stockService->load();
        $this->brandService->load();

        return $this->redirect('/');
    }
}
