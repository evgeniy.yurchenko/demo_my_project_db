<?php

namespace App\Services;

use App\Entity\OrderState;
use Doctrine\ORM\EntityManagerInterface;
use PrestaShopWebserviceException;

class OrderStateService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Update webservice "order_states" from local database
     *
     * @param $ids
     * @throws PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {

            sleep(1);

            $state = $this->entityManager->getRepository(OrderState::class)->find($id);

            $xml = $this->webService->get([
                'resource' => 'order_states',
                'id' => (int)$state->getIdState()
            ]);

            $stateFields = $xml->order_state->children();

            $stateFields->unremovable = $state->getUnremovable();
            $stateFields->delivery = $state->getDelivery();
            $stateFields->hidden = $state->getHidden();
            $stateFields->send_email = $state->getSendEmail();
            $stateFields->invoice = $state->getInvoice();
            $stateFields->color = $state->getColor();
            $stateFields->logable = $state->getLogable();
            $stateFields->shipped = $state->getShipped();
            $stateFields->paid = $state->getPaid();
            $stateFields->pdf_delivery = $state->getPdfDelivery();
            $stateFields->pdf_invoice = $state->getPdfInvoice();
            $stateFields->deleted = $state->getDeleted();
            $stateFields->name->language[0] = $state->getName();

            sleep(1);

            $this->webService->edit([
                'resource' => 'order_states',
                'id' => (int)$stateFields->id,
                'putXml' => $xml->asXML()
            ]);

        }
    }

    /**
     * Load data from webservice "order_states" to local database
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'order_states',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $states = $xml->order_states->children();

            foreach ($states as $state) {
                // TODO :Implement dedicated id`s(now just hot fix)
                $foundState = $this->entityManager->getRepository(OrderState::class)
                    ->find((int)$state->id);
//                    ->findOneBy([
//                        'id_state' => (int)$state->id
//                    ]);

                $newState = $foundState ? $foundState : new OrderState;

                $newState->setId((int)$state->id);
                $newState->setIdState((int)$state->id);
                $newState->setUnremovable((bool)(int)$state->unremovable);
                $newState->setDelivery((bool)(int)$state->delivery);
                $newState->setHidden((bool)(int)$state->hidden);
                $newState->setSendEmail((bool)(int)$state->send_email);
                $newState->setInvoice((bool)(int)$state->invoice);
                $newState->setColor($state->color);
                $newState->setLogable((bool)(int)$state->logable);
                $newState->setShipped((bool)(int)$state->shipped);
                $newState->setPaid((bool)(int)$state->paid);
                $newState->setPdfDelivery((bool)(int)$state->pdf_delivery);
                $newState->setPdfInvoice((bool)(int)$state->pdf_invoice);
                $newState->setDeleted((bool)(int)$state->deleted);
                $newState->setName($state->name->language[0]);

                $this->entityManager->persist($newState);
            }

            $this->entityManager->flush();
            $offset += 1000;
        } while ($states);
    }

    /**
     * Update all fields on webservice "order_states" from local database
     */
    public function updateAll()
    {
        $states = $this->entityManager
            ->getRepository(OrderState::class)
            ->findAll();

        foreach ($states as $state) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'order_states',
                'id' => (int)$state->getIdState()
            ]);

            $stateFields = $xml->order_state->children();

            $stateFields->unremovable = $state->getUnremovable();
            $stateFields->delivery = $state->getDelivery();
            $stateFields->hidden = $state->getHidden();
            $stateFields->send_email = $state->getSendEmail();
            $stateFields->invoice = $state->getInvoice();
            $stateFields->color = $state->getColor();
            $stateFields->logable = $state->getLogable();
            $stateFields->shipped = $state->getShipped();
            $stateFields->paid = $state->getPaid();
            $stateFields->pdf_delivery = $state->getPdfDelivery();
            $stateFields->pdf_invoice = $state->getPdfInvoice();
            $stateFields->deleted = $state->getDeleted();
            $stateFields->name->language[0] = $state->getName();

            sleep(1);

            $this->webService->edit([
                'resource' => 'order_states',
                'id' => (int)$stateFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}