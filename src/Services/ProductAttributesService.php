<?php


namespace App\Services;


use App\Entity\Product;
use App\Entity\ProductAttribute;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Session\Session;

class ProductAttributesService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService(true);
    }

    /**
     * Load data from webservice "product_attributes" to local database
     * @return mixed
     * @throws Exception
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'combinations',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $combinations = $xml->combinations->children();

            $productSession = new Session;

            foreach ($combinations as $productAttribute) {
                $foundProductAttribute = $this->entityManager->getRepository(ProductAttribute::class)
                    ->findOneBy([
                        'id_product_attribute' => $productAttribute->id
                    ]);

                $newProductAttributes = $foundProductAttribute ? $foundProductAttribute : new ProductAttribute;

                /**
                 * @var $product Product
                 */
                $product = $this->entityManager->getRepository(Product::class)
                    ->findOneBy([
                        'id_product' => $productAttribute->id_product
                    ]);

                if (!$product) {
                    if (count($productSession->getFlashBag()->get('warning')) === 0) {
                        $productSession->getFlashBag()->add('warning', 'Please, add all products');
                    }
                    continue;
                }

                $newProductAttributes->setProduct($product);
                $newProductAttributes->setIdProductAttribute((int)$productAttribute->id);
                $newProductAttributes->setReference($productAttribute->reference);
                $newProductAttributes->setSupplierReference($productAttribute->supplier_reference);
                $newProductAttributes->setLocation($productAttribute->location);
                $newProductAttributes->setEan13($productAttribute->ean13);
                $newProductAttributes->setIsbn($productAttribute->isbn);
                $newProductAttributes->setUpc($productAttribute->upc);
                $newProductAttributes->setWholesalePrice($productAttribute->wholesale_price);
                $newProductAttributes->setPrice($productAttribute->price);
                $newProductAttributes->setEcotax($productAttribute->ecotax);
                $newProductAttributes->setQuantity((int)$productAttribute->quantity);
                $newProductAttributes->setWeight($productAttribute->weight);
                $newProductAttributes->setUnitPriceImpact($productAttribute->unit_price_impact);
                $newProductAttributes->setDefaultOn((bool)(int)$productAttribute->default_on);
                $newProductAttributes->setMinimalQuantity((int)$productAttribute->minimal_quantity);
                $newProductAttributes->setLowStockThreshold((int)$productAttribute->low_stock_threshold);
                $newProductAttributes->setLowStockAlert((int)$productAttribute->low_stock_alert);
                $newProductAttributes->setIdProduct((int)$productAttribute->id_product);
                $newProductAttributes->setAvailableDate(new DateTime);
                $newProductAttributes->setDateUpd(new DateTime);

                $newProductAttributes->setProduct($product);

                $this->entityManager->persist($newProductAttributes);
            }
            $this->entityManager->flush();

            $offset += 1000;
        } while ($combinations);
    }

    /**
     * Update all fields on webservice "product_attributes" from local database
     * @throws Exception
     */
    public function updateAll()
    {
        $productAttributes = $this->entityManager
            ->getRepository(ProductAttribute::class)
            ->findAll();

        /** @var ProductAttribute $productAttribute */
        foreach ($productAttributes as $productAttribute) {

            $product = $this->entityManager->getRepository(Product::class)->find($productAttribute->getProduct());

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'combinations',
                'id' => (int)$productAttribute->getIdProductAttribute()
            ]);

            $productAttributeFields = $xml->combination->children();

            $productAttributeFields->id_product = $product->getIdProduct();
            $productAttributeFields->reference = $productAttribute->getReference();
            $productAttributeFields->supplier_reference = $productAttribute->getSupplierReference();
            $productAttributeFields->location = $productAttribute->getLocation();
            $productAttributeFields->ean13 = $productAttribute->getEan13();
            $productAttributeFields->isbn = $productAttribute->getIsbn();
            $productAttributeFields->upc = $productAttribute->getUpc();
            $productAttributeFields->wholesale_price = $productAttribute->getWholesalePrice();
            $productAttributeFields->price = $productAttribute->getPrice();
            $productAttributeFields->quantity = $productAttribute->getQuantity();
            $productAttributeFields->weight = $productAttribute->getWeight();
            $productAttributeFields->unit_price_impact = $productAttribute->getUnitPriceImpact();
            $productAttributeFields->default_on = (bool)(int)$productAttribute->getDefaultOn();
            $productAttributeFields->minimal_quantity = $productAttribute->getMinimalQuantity();
            $productAttributeFields->low_stock_threshold = $productAttribute->getLowStockThreshold();
            $productAttributeFields->low_stock_alert = (bool)(int)$productAttribute->getLowStockAlert();
            $productAttributeFields->ecotax = $productAttribute->getEcotax();
//            $productAttributeFields->available_date = $productAttribute->getAvailableDate()->format('Y-m-d');

            sleep(1);

            $this->webService->edit([
                'resource' => 'combinations',
                'id' => (int)$productAttributeFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}