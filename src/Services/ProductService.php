<?php

namespace App\Services;


use App\Entity\Brand;
use App\Entity\Product;
use App\Entity\Supplier;
use Doctrine\ORM\EntityManagerInterface;
use PrestaShopWebserviceException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ProductService
{
    protected $parameterBag;
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService, ParameterBagInterface $parameterBag)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService(true);
        $this->parameterBag = $parameterBag;
    }

    /**
     * Load data from webservice "products" to local database
     *
     * @throws PrestaShopWebserviceException
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'products',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);
            $products = $xml->products->children();

            foreach ($products as $product) {
                $foundProduct = $this->entityManager->getRepository(Product::class)
                    ->findOneBy([
                        'id_product' => (int)$product->id
                    ]);
                $newProduct = $foundProduct ? $foundProduct : new Product;

                if ((int)$product->id_supplier) {

                    /* @var Supplier $supplier */
                    $supplier = $this->entityManager->getRepository(Supplier::class)
                        ->findOneBy(["id_supplier" => (int)$product->id_supplier]);

                    $newProduct->setSupplier($supplier);
                }

                $newProduct->setIdProduct((int)$product->id);
                $newProduct->setIdSupplier((int)$product->id_supplier);
                $newProduct->setIdCategoryDefault((int)$product->id_category_default);
                $newProduct->setIdTaxRulesGroup((int)$product->id_tax_rules_group);
                $newProduct->setName($product->name->children());
                $newProduct->setEan13($product->ean13);
                $newProduct->setIsbn($product->isbn);
                $newProduct->setUpc($product->upc);
                $newProduct->setEcotax($product->ecotax);
                $newProduct->setQuantity((int)$product->quantity);
                $newProduct->setMinimalQuantity((int)$product->minimal_quantity);
                $newProduct->setPrice($product->price);
                $newProduct->setWholesalePrice($product->wholesale_price);
                $newProduct->setReference($product->reference);
                $newProduct->setSupplierReference($product->supplier_reference);
                $newProduct->setLocation($product->location);
                $newProduct->setWidth($product->width);
                $newProduct->setHeight($product->height);
                $newProduct->setDepth($product->depth);
                $newProduct->setWeight($product->weight);
                $newProduct->setOutOfStock((int)$product->delivery_out_stock);

                if ((int)$product->id_default_image) {
                    $idImageArray = str_split($product->id_default_image);
                    $link = $this->parameterBag->get('WSERVICE_URL') . "/img/p";

                    foreach ($idImageArray as $digit) {
                        $link .= "/" . $digit;
                    }

                    $link .= "/" . $product->id_default_image . "-home_default.jpg";

                    $newProduct->setImage($link);
                }

                $this->entityManager->persist($newProduct);
            }

            $this->entityManager->flush();
            $offset += 1000;

        } while ($products);

    }

    /**
     * Update all fields on webservice "products" from local database
     * @param $ids
     * @throws PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {

            sleep(1);

            /** @var Product $product */
            $product = $this->entityManager->getRepository(Product::class)->find($id);

            $xml = $this->webService->get([
                'resource' => 'products',
                'id' => (int)$product->getIdProduct()
            ]);

            $productFields = $xml->product->children();

            $productFields->id_supplier = $product->getSupplier()->getIdSupplier();

            $manufacturerId = $product->getManufacturer() ? $product->getManufacturer()->getIdManufacturer() : 0;
            $productFields->id_manufacturer = $manufacturerId;

            $productFields->id_category_default = $product->getIdCategoryDefault();
            $productFields->id_tax_rules_group = $product->getIdTaxRulesGroup();
            $productFields->reference = $product->getReference();
            $productFields->supplier_reference = $product->getSupplierReference();
            $productFields->location = $product->getLocation();
            $productFields->width = $product->getWidth();
            $productFields->height = $product->getHeight();
            $productFields->depth = $product->getDepth();
            $productFields->weight = $product->getWeight();
            $productFields->ean13 = $product->getEan13();
            $productFields->isbn = $product->getIsbn();
            $productFields->upc = $product->getUpc();
            $productFields->ecotax = $product->getEcotax();
            $productFields->minimal_quantity = $product->getMinimalQuantity();
            $productFields->price = $product->getPrice();
            $productFields->wholesale_price = $product->getWholesalePrice();
            $productFields->name->language[0] = $product->getName();

            unset($productFields->manufacturer_name);
            unset($productFields->quantity);

            sleep(1);

            $this->webService->edit([
                'resource' => 'products',
                'id' => (int)$productFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }
}