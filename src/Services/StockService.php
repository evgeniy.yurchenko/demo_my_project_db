<?php

namespace App\Services;


use App\Entity\Product;
use App\Entity\ProductAttribute;
use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class StockService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Load data from webservice "stocks" to local database
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'stock_availables',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $stocks = $xml->stock_availables->children();

            $productSession = new Session;

            foreach ($stocks as $stock) {
                $foundStock = $this->entityManager->getRepository(Stock::class)
                    ->findOneBy([
                        'stock_id' => (int)$stock->id
                    ]);

                /**
                 * @var $product Product
                 */
                $product = $this->entityManager->getRepository(Product::class)
                    ->findOneBy([
                        'id_product' => (int)$stock->id_product
                    ]);

                if (!$product) {
                    if (count($productSession->getFlashBag()->get('warning')) === 0) {
                        $productSession->getFlashBag()->add('warning', 'Please, add all products');
                    }
                    continue;
                }

                /**
                 * @var $productAttribute ProductAttribute
                 */
                $productAttribute = $this->entityManager->getRepository(ProductAttribute::class)
                    ->findOneBy([
                        'id_product_attribute' => (int)$stock->id_product_attribute
                    ]);

                $newStock = $foundStock ? $foundStock : new Stock();

                $newStock->setProduct($product);
                $newStock->setProductAttribute($productAttribute);
                $newStock->setStockId((int)$stock->id);
                $newStock->setIdProduct((int)$stock->id_product);
                $newStock->setIdProductAttribute((int)$stock->id_product_attribute);
                $newStock->setIdShop((int)$stock->id_shop);
                $newStock->setIdShopGroup((int)$stock->id_shop_group);
                $newStock->setQuantity((int)$stock->quantity);
                $newStock->setDependsOnStock((int)$stock->depends_of_stock);
                $newStock->setOutOfStock((int)$stock->out_of_stock);
                $newStock->setLocation($stock->location);

                $this->entityManager->persist($newStock);
            }
            $this->entityManager->flush();
            $offset += 1000;
        } while ($stocks);
    }

    /**
     * Update all fields on webservice "stocks" from local database
     */
    public function updateAll()
    {
        $stocks = $this->entityManager
            ->getRepository(Stock::class)
            ->findAll();

        /** @var Stock $stock */
        foreach ($stocks as $stock) {
            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'stock_availables',
                'id' => (int)$stock->getStockId()
            ]);

            $stockFields = $xml->stock_available->children();

            $stockFields->quantity = $stock->getQuantity();
            $stockFields->depends_on_stock = $stock->getDependsOnStock();
            $stockFields->out_of_stock = $stock->getOutOfStock();
            $stockFields->location = $stock->getLocation();
            $stockFields->id_product = $stock->getProduct()->getIdProduct();
            $stockFields->id_product_attribute = $stock->getProductAttribute() ? $stock->getProductAttribute()->getIdProductAttribute() : 0;

            sleep(1);

            $this->webService->edit([
                'resource' => 'stock_availables',
                'id' => (int)$stockFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}
