<?php

namespace App\Services;

use App\Entity\Carrier;
use Doctrine\ORM\EntityManagerInterface;
use PrestaShopWebserviceException;

class CarrierService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Update selected carriers from local database to webservice
     *
     * @param $ids
     * @throws PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {

            sleep(1);

            $carrier = $this->entityManager->getRepository(Carrier::class)->find($id);

            $xml = $this->webService->get([
                'resource' => 'carriers',
                'id' => (int)$carrier->getIdCarrier()
            ]);

            $carrierFields = $xml->carrier->children();

            $carrierFields->id_reference = $carrier->getIdReference();
            $carrierFields->id_tax_rules_group = $carrier->getIdTaxRulesGroup();
            $carrierFields->name = $carrier->getName();
            $carrierFields->url = $carrier->getUrl();
            $carrierFields->active = (int)$carrier->getActive();
            $carrierFields->is_free = $carrier->getIsFree();
            $carrierFields->max_width = $carrier->getMaxWidth();
            $carrierFields->max_height = $carrier->getMaxHeight();
            $carrierFields->max_depth = $carrier->getMaxDepth();
            $carrierFields->max_weight = $carrier->getMaxWeight();
            $carrierFields->delay->language[0] = $carrier->getDelay();

            sleep(1);

            $this->webService->edit([
                'resource' => 'carriers',
                'id' => (int)$carrierFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

    /**
     * Load carriers from webservice to local database
     */
    public function load()
    {
        $xml = $this->webService->get([
            'resource' => 'carriers',
            'display' => 'full',
        ]);

        $carriers = $xml->carriers->children();

        foreach ($carriers as $carrier) {
            $foundCarrier = $this->entityManager->getRepository(Carrier::class)
                ->findOneBy([
                    'id_carrier' => (int)$carrier->id
                ]);

            $newCarrier = $foundCarrier ? $foundCarrier : new Carrier;

            $newCarrier->setIdCarrier((int)$carrier->id);
            $newCarrier->setIdReference((int)$carrier->id_reference);
            $newCarrier->setIdTaxRulesGroup((int)$carrier->id_tax_rules_group);
            $newCarrier->setName($carrier->name);
            $newCarrier->setUrl($carrier->url);
            $newCarrier->setActive((bool)(int)$carrier->active);
            $newCarrier->setIsFree((bool)(int)$carrier->is_free);
            $newCarrier->setMaxWidth((int)$carrier->max_width);
            $newCarrier->setMaxHeight((int)$carrier->max_height);
            $newCarrier->setMaxDepth((int)$carrier->max_depth);
            $newCarrier->setMaxWeight((int)$carrier->max_weight);
            $newCarrier->setDelay($carrier->delay->language[0]);

            $this->entityManager->persist($newCarrier);
        }

        $this->entityManager->flush();
    }

    /**
     * Update all fields on webservice "carrier" from local database
     */
    public function updateAll()
    {
        $carriers = $this->entityManager
            ->getRepository(Carrier::class)
            ->findAll();

        foreach ($carriers as $carrier) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'carriers',
                'id' => (int)$carrier->getIdCarrier()
            ]);

            $carrierFields = $xml->carrier->children();

            $carrierFields->id_reference = $carrier->getIdReference();
            $carrierFields->id_tax_rules_group = $carrier->getIdTaxRulesGroup();
            $carrierFields->name = $carrier->getName();
            $carrierFields->url = $carrier->getUrl();
            $carrierFields->active = $carrier->getActive();
            $carrierFields->is_free = $carrier->getIsFree();
            $carrierFields->max_width = $carrier->getMaxWidth();
            $carrierFields->max_height = $carrier->getMaxHeight();
            $carrierFields->max_depth = $carrier->getMaxDepth();
            $carrierFields->max_weight = $carrier->getMaxWeight();
            $carrierFields->delay->language[0] = $carrier->getDelay();

            sleep(1);

            $this->webService->edit([
                'resource' => 'carriers',
                'id' => (int)$carrierFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}