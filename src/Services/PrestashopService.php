<?php

namespace App\Services;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PrestashopService
{


    protected $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;

    }

    public function webService($debug = false)
    {
        $url = $this->parameterBag->get('WSERVICE_URL');

        $key = $this->parameterBag->get('WSERVICE_KEY');

        try {
            return new \PrestaShopWebservice($url, $key, $debug);
        } catch (\PrestaShopWebserviceException $exception) {
            echo 'Other error: <br />' . $exception->getMessage();
        }

    }
}