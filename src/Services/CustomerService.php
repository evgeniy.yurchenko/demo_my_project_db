<?php


namespace App\Services;


use App\Entity\Customer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PrestaShopWebserviceException;

class CustomerService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Update webservice "customers" from local database
     * @param $ids
     * @throws PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {
            $customer = $this->entityManager->getRepository(Customer::class)->find($id);

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'customers',
                'id' => (int)$customer->getIdCustomer()
            ]);

            $customerFields = $xml->customer->children();

            $customerFields->id_shop_group = $customer->getIdShopGroup();
            $customerFields->id_gender = $customer->getIdGender();
            $customerFields->id_default_group = $customer->getIdDefaultGroup();
            $customerFields->id_lang = $customer->getIdLang();
            $customerFields->company = $customer->getCompany();
            $customerFields->siret = $customer->getSiret();
            $customerFields->firstname = $customer->getFirstname();
            $customerFields->lastname = $customer->getLastname();
            $customerFields->email = $customer->getEmail();
            $customerFields->passwd = $customer->getPasswd();
            $customerFields->birthday = $customer->getBirthday()->format('Y-m-d');;
            $customerFields->newsletter = $customer->getNewsletter();
            $customerFields->is_guest = (bool)(int)$customer->getIsGuest();
            $customerFields->active = (bool)(int)$customer->getActive();

            sleep(1);

            $this->webService->edit([
                'resource' => 'customers',
                'id' => (int)$customerFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

    /**
     * Load data from webservice "customers" to local database
     *
     * @throws Exception
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'customers',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $customers = $xml->customers->children();

            foreach ($customers as $customer) {

                $foundCustomer = $this->entityManager->getRepository(Customer::class)
                    ->findOneBy([
                        'id_customer' => (int)$customer->id
                    ]);

                $newCustomers = $foundCustomer ? $foundCustomer : new Customer;

                $newCustomers->setIdCustomer((int)$customer->id);
                $newCustomers->setIdShopGroup((int)$customer->id_shop_group);
                $newCustomers->setIdDefaultGroup((int)$customer->id_default_group);
                $newCustomers->setIdGender((int)$customer->id_gender);
                $newCustomers->setIdLang((int)$customer->id_lang);
                $newCustomers->setCompany($customer->company);
                $newCustomers->setSiret($customer->siret);
                $newCustomers->setApe($customer->ape);
                $newCustomers->setFirstname($customer->firstname);
                $newCustomers->setLastname($customer->lastname);
                $newCustomers->setEmail($customer->email);
                $newCustomers->setPasswd($customer->passwd);
                $newCustomers->setBirthday(new DateTime("2000-11-06"));
                $newCustomers->setNewsletter((bool)(int)$customer->newsletter);
                $newCustomers->setIsGuest((bool)(int)$customer->is_guest);
                $newCustomers->setActive((bool)(int)$customer->active);

                $this->entityManager->persist($newCustomers);
            }

            $this->entityManager->flush();
            $offset += 1000;

        } while ($customers);
    }

    /**
     * Update all fields on webservice "customers" from local database
     */
    public function updateAll()
    {
        $customers = $this->entityManager
            ->getRepository(Customer::class)
            ->findAll();

        foreach ($customers as $customer) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'customers',
                'id' => (int)$customer->getIdCustomer()
            ]);

            $customerFields = $xml->customer->children();

            $customerFields->id_shop_group = $customer->getIdShopGroup();
            $customerFields->id_gender = $customer->getIdGender();
            $customerFields->id_default_group = $customer->getIdDefaultGroup();
            $customerFields->id_lang = $customer->getIdLang();
            $customerFields->company = $customer->getCompany();
            $customerFields->siret = $customer->getSiret();
            $customerFields->ape = $customer->getApe();
            $customerFields->firstname = $customer->getFirstname();
            $customerFields->lastname = $customer->getLastname();
            $customerFields->email = $customer->getEmail();
            $customerFields->passwd = $customer->getPasswd();
            $customerFields->birthday = $customer->getBirthday()->format('Y-m-d');;
            $customerFields->newsletter = $customer->getNewsletter();
            $customerFields->is_guest = $customer->getIsGuest();
            $customerFields->active = $customer->getActive();

            sleep(1);

            $this->webService->edit([
                'resource' => 'customers',
                'id' => (int)$customerFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}