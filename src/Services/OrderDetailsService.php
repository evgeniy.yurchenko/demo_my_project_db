<?php

namespace App\Services;

use App\Entity\OrderDetails;
use Doctrine\ORM\EntityManagerInterface;

class OrderDetailsService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    // TODO: Need to finish

    /**
     * Load data from webservice "order_details" to local database
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'order_details',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $details = $xml->order_details->children();

            foreach ($details as $detail) {

                $foundDetail = $this->entityManager->getRepository(OrderDetails::class)
                    ->findOneBy([
                        'id_order_detail' => (int)$detail->id
                    ]);

                $newDetail = $foundDetail ? $foundDetail : new OrderDetails();

                $newDetail->setConnectedOrder();
                $newDetail->setConnectedProduct();
                $newDetail->setConnectedProductAttribute();
                $newDetail->setIdOrder();
                $newDetail->setIdOrderDetail();
                $newDetail->setProductId();
                $newDetail->setProductAttributeId();
                $newDetail->setProductQuantityReinjected();
                $newDetail->setGroupReduction();
                $newDetail->setDiscountQuantityApplied();
                $newDetail->setIdOrderInvoice();
                $newDetail->setIdWarehouse();
                $newDetail->setIdShop();
                $newDetail->setIdCustomization();
                $newDetail->setProductQuantity();
                $newDetail->setProductQuantityInStock();
                $newDetail->setProductQuantityReturn();
                $newDetail->setProductQuantityRefunded();
                $newDetail->setProductPrice();
                $newDetail->setReductionAmount();
                $newDetail->setReductionAmountTaxIncl();
                $newDetail->setReductionAmountTaxExcl();
                $newDetail->setProductQuantityDiscount();
                $newDetail->setProductWeight();
                $newDetail->setTaxComputationMethod();
                $newDetail->setIdTaxRulesGroup();
                $newDetail->setEcotax();
                $newDetail->setEcotaxTaxRate();
                $newDetail->setDownloadNb();
                $newDetail->setUnitPriceTaxIncl();
                $newDetail->setUnitPriceTaxExcl();
                $newDetail->setTotalPriceTaxIncl();
                $newDetail->setTotalPriceTaxExcl();
                $newDetail->setTotalShippingPriceTaxIncl();
                $newDetail->setTotalShippingPriceTaxExcl();
                $newDetail->setPurchaseSupplierPrice();
                $newDetail->setOriginalProductPrice();
                $newDetail->setOriginalWholesalePrice();

                $this->entityManager->persist($newDetail);
            }
            $this->entityManager->flush();
            $offset += 1000;

        } while ($details);
    }

}