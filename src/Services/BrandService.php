<?php

namespace App\Services;

use App\Entity\Brand;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Validator\Constraints\Date;

class BrandService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Load brands from webservice to local database
     * @throws Exception
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'manufacturers',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $manufacturers = $xml->manufacturers->children();

            foreach ($manufacturers as $brand) {
                $foundBrand = $this->entityManager->getRepository(Brand::class)
                    ->findOneBy([
                        'id_manufacturer' => $brand->id
                    ]);

                $newBrand = $foundBrand ? $foundBrand : new Brand;

                $newBrand->setName($brand->name);
                $newBrand->setActive((bool)(int)$brand->active);
                $date_add = new DateTime($brand->date_add);
                $date_upd = new DateTime($brand->date_upd);
                $newBrand->setDateUpd($date_upd);
                $newBrand->setDateAdd($date_add);
                $newBrand->setIdManufacturer((int)$brand->id);
                $newBrand->setShortDescription($brand->short_description);

                $this->entityManager->persist($newBrand);
            }

            $this->entityManager->flush();

            $offset += 1000;

        } while ($manufacturers);

    }

    /**
     * Update all brands from local database to webservice
     * @throws Exception
     */
    public function updateAll()
    {
        $brands = $this->entityManager->getRepository(Brand::class)
            ->findAll();

        foreach ($brands as $brand) {
            $xml = $this->webService->get([
                'resource' => 'manufacturers',
                'id' => (int)$brand->getIdManufacturer()
            ]);

            $brandField = $xml->manufacturer->children();

            $brandField->active = $brand->getActive();
            $brandField->name = $brand->getName();
            $brandField->short_description = $brand->getShortDescription();
            unset($brandField->link_rewrite);
            $this->webService->edit([
                'resource' => 'manufacturers',
                'id' => (int)$brandField->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}