<?php


namespace App\Services;


use App\Entity\Address;
use App\Entity\Carrier;
use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\ProductAttribute;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Session\Session;

class CartService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Load data from webservice "carts" to local database
     *
     * @throws Exception
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'carts',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $carts = $xml->carts->children();

            $carrierSession = new Session;
            $customerSession = new Session;
            $addressDeliverySession = new Session;
            $addressInvoiceSession = new Session;
            $productSession = new Session;
            $productAttributeSession = new Session;


            foreach ($carts as $cart) {

                $foundCart = $this->entityManager->getRepository(Cart::class)
                    ->findOneBy([
                        'id_cart' => $cart->id
                    ]);

                /* @var $newCart Cart */
                $newCart = $foundCart ? $foundCart : new Cart;

                /* @var $carrier Carrier */
                $carrier = $this->entityManager->getRepository(Carrier::class)
                    ->findOneBy([
                        'id_carrier' => $cart->id_carrier
                    ]);

                if (!$carrier) {
                    if (count($carrierSession->getFlashBag()->get('warning')) === 0) {
                        $carrierSession->getFlashBag()->add('warning', 'Please, add all carriers');
                    }
                    continue;
                }

                /* @var $customer Customer */
                $customer = $this->entityManager->getRepository(Customer::class)
                    ->findOneBy([
                        'id_customer' => $cart->id_customer
                    ]);

                if (!$customer) {
                    if (count($customerSession->getFlashBag()->get('warning')) === 0) {
                        $customerSession->getFlashBag()->add('warning', 'Please, add all customers');
                    }
                    continue;
                }

                /* @var Address $addressDelivery */
                $addressDelivery = $this->entityManager
                    ->getRepository(Address::class)
                    ->findOneBy([
                        'id_address' => $cart->id_address_delivery
                    ]);

                if (!$addressDelivery) {
                    if (count($addressDeliverySession->getFlashBag()->get('warning')) === 0) {
                        $addressDeliverySession->getFlashBag()->add('warning', 'Please, add all address deliveries');
                    }
                    continue;
                }

                /* @var Address $addressInvoice */
                $addressInvoice = $this->entityManager
                    ->getRepository(Address::class)
                    ->findOneBy([
                        'id_address' => $cart->id_address_invoice
                    ]);

                if (!$addressInvoice) {
                    if (count($addressInvoiceSession->getFlashBag()->get('warning')) === 0) {
                        $addressInvoiceSession->getFlashBag()->add('warning', 'Please, add all address invoices');
                    }
                    continue;
                }

                $newCart->setCarrier($carrier);
                $newCart->setCusomer($customer);
                $newCart->setAddressDelivery($addressDelivery);
                $newCart->setAddressInvoice($addressInvoice);
                $newCart->setIdCart((int)$cart->id);
                $newCart->setIdShopGroup((int)$cart->id_shop_group);
                $newCart->setIdShop((int)$cart->id_shop);
                $newCart->setDeliveryOption($cart->delivery_option);
                $newCart->setIdLang((int)$cart->id_lang);
                $newCart->setIdGuest((int)$cart->id_guest);
                $newCart->setSecureKey($cart->secure_key);
                $newCart->setRecyclable((bool)(int)$cart->recyclable);
                $newCart->setGift((bool)(int)$cart->gift);
                $newCart->setGiftMessage($cart->gift_message);
                $date_add = new DateTime($cart->date_add);
                $newCart->setDateAdd($date_add);
                $newCart->setDateUpd(new DateTime);

                $this->entityManager->persist($newCart);

                $cartProducts = $cart->associations->cart_rows->children();

                foreach ($cartProducts as $cartProduct) {
                    $foundCartProduct = $this->entityManager->getRepository(CartProduct::class)
                        ->findOneBy([
                            'id_cart' => $cart->id,
                            'id_product' => $cartProduct->id_product,
                            'id_product_attribute' => $cartProduct->id_product_attribute,
                        ]);

                    /* @var $newCartProduct CartProduct */
                    $newCartProduct = $foundCartProduct ? $foundCartProduct : new CartProduct;

                    /* @var $product Product */
                    $product = $this->entityManager->getRepository(Product::class)
                        ->findOneBy([
                            'id_product' => $cartProduct->id_product
                        ]);

                    if (!$product) {
                        if (count($productSession->getFlashBag()->get('warning')) === 0) {
                            $productSession->getFlashBag()->add('warning', 'Please, add all products');
                        }
                        continue;
                    }

                    /* @var $productAttribute ProductAttribute */
                    $productAttribute = $this->entityManager->getRepository(ProductAttribute::class)
                        ->findOneBy([
                            'id_product_attribute' => $cartProduct->id_product_attribute
                        ]);

                    if (!$productAttribute) {
                        if (count($productAttributeSession->getFlashBag()->get('warning')) === 0) {
                            $productAttributeSession->getFlashBag()->add('warning', 'Please, add all product attributes');
                        }
                        continue;
                    }

                    /* @var Address $addressDelivery */
                    $addressDelivery = $this->entityManager
                        ->getRepository(Address::class)
                        ->findOneBy([
                            'id_address' => $cartProduct->id_address_delivery
                        ]);

                    if (!$addressDelivery) {
                        if (count($addressDeliverySession->getFlashBag()->get('warning')) === 0) {
                            $addressDeliverySession->getFlashBag()->add('warning', 'Please, add all address deliveries');
                        }
                        continue;
                    }

                    $newCartProduct->setCart($newCart);
                    $newCartProduct->setIdCart((int)$cart->id);
                    $newCartProduct->setProduct($product);
                    $newCartProduct->setIdProduct((int)$cartProduct->id_product);
                    $newCartProduct->setProductAttribute($productAttribute);
                    $newCartProduct->setIdProductAttribute((int)$cartProduct->id_product_attribute);
                    $newCartProduct->setAddressDelivery($addressDelivery);
                    $newCartProduct->setIdAddressDelivery((int)$cartProduct->id_address_delivery);
                    $newCartProduct->setIdCustomization((int)$cartProduct->id_customization);
                    $newCartProduct->setIdShop((int)$cart->id_shop);
                    $newCartProduct->setQuantity((int)$cartProduct->quantity);

                    $this->entityManager->persist($newCartProduct);
                }
            }
            $this->entityManager->flush();
            $offset += 1000;
        } while ($carts);
    }

    public function updateAll()
    {
        $carts = $this->entityManager
            ->getRepository(Cart::class)
            ->findAll();

        foreach ($carts as $cart) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'carts',
                'id' => $cart->getIdCart()
            ]);

            $cartFields = $xml->cart->children();

            $cartFields->id_carrier = $cart->getCarrier()->getIdCarrier();
            $cartFields->id_customer = $cart->getCusomer()->getIdCustomer();
            $cartFields->id_address_delivery = $cart->getAddressDelivery()->getIdAddress();
            $cartFields->id_address_invoice = $cart->getAddressInvoice()->getIdAddress();
            $cartFields->id_shop_group = $cart->getIdShopGroup();
            $cartFields->id_shop = $cart->getIdShop();
            $cartFields->delivery_option = $cart->getDeliveryOption();
            $cartFields->id_lang = $cart->getIdLang();
            $cartFields->id_guest = $cart->getIdGuest();
            $cartFields->secure_key = $cart->getSecureKey();
            $cartFields->recyclable = $cart->getRecyclable();
            $cartFields->gift = $cart->getGift();
            $cartFields->gift_message = $cart->getGiftMessage();
            $date = new DateTime();
            $cartFields->date_upd = $date->format('Y-m-d');

            sleep(1);

            // TODO: Final edit cart_product if it's necessary

            $this->webService->edit([
                'resource' => 'carts',
                'id' => (int)$cartFields->id,
                'putXml' => $xml->asXML()
            ]);

        }
    }

}
