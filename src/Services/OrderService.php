<?php


namespace App\Services;


use App\Entity\Address;
use App\Entity\Carrier;
use App\Entity\Cart;
use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Entity\OrderState;
use App\Entity\Product;
use App\Entity\ProductAttribute;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PrestaShopWebserviceException;
use Symfony\Component\HttpFoundation\Session\Session;

class OrderService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Update webservice "orders" from local database
     *
     * @param $ids
     * @throws PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {
            sleep(1);

            $orders = $this->entityManager->getRepository(Order::class)->find($id);

            $xml = $this->webService->get([
                'resource' => 'orders',
                'id' => $orders->getIdOrder()
            ]);

            $orderFields = $xml->order->children();

            $orderFields->reference = $orders->getReference();
            $orderFields->id_currency = $orders->getIdCurrency();
            $orderFields->id_address_delivery = $orders->getIdAddressDelivery();
            $orderFields->id_address_invoice = $orders->getIdAddressInvoice();
            $orderFields->current_state = $orders->getCurrentState();
            $orderFields->secure_key = $orders->getSecureKey();
            $orderFields->payment = $orders->getPayment();
            $orderFields->conversion_rate = $orders->getConversionRate();
            $orderFields->recyclable = $orders->getRecyclable();
            $orderFields->gift = $orders->getGift();
            $orderFields->shipping_number = $orders->getShippingNumber();
            $orderFields->total_discounts = $orders->getTotalDiscounts();
            $orderFields->id_cart = $orders->getIdCart();

            sleep(1);

            $this->webService->edit([
                'resource' => 'orders',
                'id' => (int)$orderFields->id,
                'putXml' => $xml->asXML(),
            ]);
        }

    }

    /**
     * Load data from webservice "orders" to local database
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'orders',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $orders = $xml->orders->children();

            $cartSession = new Session;
            $addressDeliverySession = new Session;
            $addressInvoiceSession = new Session;
            $customerSession = new Session;
            $stateSession = new Session;
            $carrierSession = new Session;
            $productSession = new Session;
            $productAttributeSession = new Session;

            foreach ($orders as $order) {
                $foundOrder = $this->entityManager->getRepository(Order::class)
                    ->findOneBy([
                        'id_order' => (int)$order->id
                    ]);

                /* @var $newOrder Order */
                $newOrder = $foundOrder ? $foundOrder : new Order;

                /**
                 * @var Cart $cartId
                 */
                $cartId = $this->entityManager->getRepository(Cart::class)
                    ->findOneBy([
                        'id_cart' => $order->id_cart
                    ]);

                // TODO: Flash messages does not work
                if (!$cartId) {
                    $cartSession->getFlashBag()->add('warning', 'Please, add all carts');
//                continue;
                }

                /**
                 * @var Address $addressDelivery
                 */
                $addressDelivery = $this->entityManager
                    ->getRepository(Address::class)
                    ->findOneBy([
                        'id_address' => $order->id_address_delivery
                    ]);

                if (!$addressDelivery) {
                    $addressDeliverySession->getFlashBag()->add('warning', 'Please, add all address deliveries');
//                    continue;
                }

                /**
                 * @var Address $addressInvoice
                 */
                $addressInvoice = $this->entityManager->getRepository(Address::class)
                    ->findOneBy([
                        'id_address' => $order->id_address_invoice
                    ]);

                if (!$addressInvoice) {
                    $addressInvoiceSession->getFlashBag()->add('warning', 'Please, add all address invoice');
//                    continue;
                }

                /**
                 * @var Customer $customer
                 */
                $customer = $this->entityManager->getRepository(Customer::class)
                    ->findOneBy([
                        'id_customer' => $order->id_customer
                    ]);

                if (!$customer) {
                    $customerSession->getFlashBag()->add('warning', 'Please, add all customers');
                    continue;
                }

                /**
                 * @var OrderState $state
                 */
                $state = $this->entityManager->getRepository(OrderState::class)
                    ->findOneBy([
                        'id_state' => $order->current_state
                    ]);

                if (!$state) {
                    $stateSession->getFlashBag()->add('warning', 'Please, add all states');
                    continue;
                }

                /**
                 * @var Carrier $carrier
                 */
                $carrier = $this->entityManager->getRepository(Carrier::class)
                    ->findOneBy([
                        'id_carrier' => $order->id_carrier
                    ]);

                if (!$carrier) {
                    $carrierSession->getFlashBag()->add('warning', 'Please, add all carriers');
//                    continue;
                }

                $newOrder->setCart($cartId);
                $newOrder->setIdCart((int)$order->id_cart);
                $newOrder->setIdAddressDelivery((int)$order->id_address_delivery);
                $newOrder->setIdAddressInvoice((int)$order->id_address_invoice);
                $newOrder->setIdOrder((int)$order->id);
                $newOrder->setReference($order->reference);
                $date_add = new DateTime($order->date_add);
                $newOrder->setDateAdd($date_add);
                $newOrder->setIdCurrency((int)$order->id_currency);
                $newOrder->setCart($cartId);
                $newOrder->setAddressDelivery($addressDelivery);
                $newOrder->setAddressInvoice($addressInvoice);
                $newOrder->setCurrentState((int)$order->current_state);
                $newOrder->setSecureKey($order->secure_key);
                $newOrder->setPayment($order->payment);
                $newOrder->setConversionRate($order->conversion_rate);
                $newOrder->setRecyclable((bool)(int)$order->recyclable);
                $newOrder->setGift((bool)(int)$order->gift);
                $newOrder->setShippingNumber((string)$order->shipping_number);
                $newOrder->setTotalDiscounts((float)$order->total_discounts);
                $newOrder->setTotalPaidTaxExcl((float)$order->total_paid_tax_excl);
                $newOrder->setTotalPaidTaxIncl((float)$order->total_paid_tax_incl);
                $newOrder->setIdCustomer((int)$order->id_customer);
                $newOrder->setIdCarrier((int)$order->id_carrier);
                $newOrder->setCarrier($carrier);
                $newOrder->setCustomer($customer);
                $newOrder->setState($state);

                $orderProducts = $order->associations->order_rows->children();

                $orderProductsWeight = 0;

                foreach ($orderProducts as $orderProduct) {
                    $foundOrderProduct = $this->entityManager->getRepository(OrderProduct::class)
                        ->findOneBy([
                            'order_id' => $order->id,
                            'id_product' => $orderProduct->product_id,
                            'id_product_attribute' => (int)$orderProduct->product_attribute_id,
                        ]);

                    /* @var OrderProduct $newOrderProduct */
                    $newOrderProduct = $foundOrderProduct ? $foundOrderProduct : new OrderProduct;

                    /* @var $product Product */
                    $product = $this->entityManager->getRepository(Product::class)
                        ->findOneBy([
                            'id_product' => $orderProduct->product_id
                        ]);

                    if (!$product) {
                        $productSession->getFlashBag()->add('warning', 'Please, add all products');
                        continue;
                    } else {
                        $orderProductsWeight += $product->getWeight() * (int)$orderProduct->product_quantity;
                    }

                    /* @var $productAttribute ProductAttribute */
                    $productAttribute = $this->entityManager->getRepository(ProductAttribute::class)
                        ->findOneBy([
                            'id_product_attribute' => $orderProduct->product_attribute_id
                        ]);

                    if (!$productAttribute) {
                        $productAttributeSession->getFlashBag()->add('warning', 'Please, add all product attributes');
//                        continue;
                    }

                    $newOrderProduct->setOrderId((int)$order->id);
                    $newOrderProduct->setIdOrderProduct((int)$orderProduct->id);
                    $newOrderProduct->setProduct($product);
                    $newOrderProduct->setIdProduct((int)$orderProduct->product_id);
                    $newOrderProduct->setProductAttribute($productAttribute);
                    $newOrderProduct->setIdProductattribute((int)$orderProduct->product_attribute_id);
                    $newOrderProduct->setOrderConnected($newOrder);
                    $newOrderProduct->setProductEan13($orderProduct->product_ean13);
                    $newOrderProduct->setProductIsbn($orderProduct->product_isbn);
                    $newOrderProduct->setProductName($orderProduct->product_name);
                    $newOrderProduct->setProductPrice($orderProduct->product_price);
                    $newOrderProduct->setProductReference($orderProduct->product_reference);
                    $newOrderProduct->setProductUpc($orderProduct->product_upc);
                    $newOrderProduct->setProductQuantity((int)$orderProduct->product_quantity);
                    $newOrderProduct->setUnitPriceTaxExcl($orderProduct->unit_price_tax_excl);
                    $newOrderProduct->setUnitPriceTaxIncl($orderProduct->unit_price_tax_incl);

                    $this->entityManager->persist($newOrderProduct);
                }

                $newOrder->setOrderWeight($orderProductsWeight);

                $this->entityManager->persist($newOrder);
            }

            $this->entityManager->flush();
            $offset += 1000;
        } while ($orders);

    }

    /**
     * Update all fields on webservice "orders" from local database
     */
    public function updateAll()
    {
        $orders = $this->entityManager
            ->getRepository(Order::class)
            ->findAll();

        foreach ($orders as $order) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'orders',
                'id' => $order->getIdOrder()
            ]);

            $orderFields = $xml->order->children();

            $orderFields->reference = $order->getReference();
            $orderFields->id_currency = $order->getIdCurrency();
            $orderFields->id_address_delivery = $order->getIdAddressDelivery();
            $orderFields->id_address_invoice = $order->getIdAddressInvoice();
            $orderFields->current_state = $order->getCurrentState();
            $orderFields->secure_key = $order->getSecureKey();
            $orderFields->payment = $order->getPayment();
            $orderFields->conversion_rate = $order->getConversionRate();
            $orderFields->recyclable = $order->getRecyclable();
            $orderFields->gift = $order->getGift();
            $orderFields->shipping_number = $order->getShippingNumber();
            $orderFields->total_discounts = $order->getTotalDiscounts();
            $orderFields->id_cart = $order->getIdCart();

            sleep(1);

            $this->webService->edit([
                'resource' => 'orders',
                'id' => (int)$orderFields->id,
                'putXml' => $xml->asXML(),
            ]);
        }
    }

}