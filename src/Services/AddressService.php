<?php

namespace App\Services;

use App\Entity\Address;
use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class AddressService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Update selected addresses from local database to webservice
     *
     * @param $ids
     * @throws \PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {
            $address = $this->entityManager->getRepository(Address::class)->find($id);

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'addresses',
                'id' => (int)$address->getIdAddress()
            ]);

            $addressFields = $xml->address->children();

            $addressFields->id_customer = $address->getIdCustomer();
            $addressFields->id_country = $address->getIdCountry();
            $addressFields->id_state = $address->getIdState();
            $addressFields->id_manufacturer = $address->getIdManufacturer();
            $addressFields->id_supplier = $address->getIdSupplier();
            $addressFields->id_warehouse = $address->getIdWarehouse();
            $addressFields->alias = $address->getAlias();
            $addressFields->company = $address->getCompany();
            $addressFields->lastname = $address->getLastname();
            $addressFields->firstname = $address->getFirstname();
            $addressFields->address1 = $address->getAddress1();
            $addressFields->address2 = $address->getAddress2();
            $addressFields->postcode = $address->getPostcode();
            $addressFields->city = $address->getCity();
            $addressFields->other = $address->getOther();
            $addressFields->phone = $address->getPhone();
            $addressFields->phone_mobile = $address->getPhoneMobile();
            $addressFields->vat_number = $address->getVatNumber();
            $addressFields->dni = $address->getDni();
            $addressFields->deleted = $address->getDeleted();

            sleep(1);

            $this->webService->edit([
                'resource' => 'addresses',
                'id' => (int)$addressFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

    /**
     * Load addresses from webservice to local database
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'addresses',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $addresses = $xml->addresses->children();
            $customerSession = new Session;
            foreach ($addresses as $address) {

                $foundAddress = $this->entityManager->getRepository(Address::class)
                    ->findOneBy([
                        'id_address' => (int)$address->id
                    ]);

                $newAddress = $foundAddress ? $foundAddress : new Address;
                $customer = null;
                if ((int)$address->id_customer !== 0) {
                    /**
                     * @var Customer $customer
                     */
                    $customer = $this->entityManager->getRepository(Customer::class)
                        ->findOneBy([
                            'id_customer' => $address->id_customer
                        ]);

                    if (!$customer) {
                        if (count($customerSession->getFlashBag()->get('warning')) === 0) {
                            $customerSession->getFlashBag()->add('warning', 'Please, add all customers');
                        }
                        continue;
                    }
                }

                sleep(0.5);

                $xml_country = $this->webService->get([
                    'resource' => 'countries',
                    'id' => (int)$address->id_country
                ]);

                $countryFields = $xml_country->country->children();

                $newAddress->setIdCustomer((int)$address->id_customer);
                $newAddress->setCustomer($customer);
                $newAddress->setIdAddress((int)$address->id);
                $newAddress->setIdCountry((int)$address->id_country);
                $newAddress->setCountry((string)$countryFields->name->language[0]);
                $newAddress->setIdState((int)$address->id_state);
                $newAddress->setIdManufacturer((int)$address->id_manufacturer);
                $newAddress->setIdSupplier((int)$address->id_supplier);
                $newAddress->setIdWarehouse((int)$address->id_warehouse);
                $newAddress->setAlias($address->alias);
                $newAddress->setCompany($address->company);
                $newAddress->setLastname($address->lastname);
                $newAddress->setFirstname($address->firstname);
                $newAddress->setAddress1($address->address1);
                $newAddress->setAddress2($address->address2);
                $newAddress->setPostcode($address->postcode);
                $newAddress->setCity($address->city);
                $newAddress->setOther($address->other);
                $newAddress->setPhone($address->phone);
                $newAddress->setPhoneMobile($address->phone_mobile);
                $newAddress->setVatNumber($address->vat_number);
                $newAddress->setDni($address->dni);
                $newAddress->setActive(true);
                $newAddress->setDeleted((bool)(int)$address->deleted);

                $this->entityManager->persist($newAddress);
            }
            $this->entityManager->flush();
            $offset += 1000;
        } while ($addresses);
    }

    /**
     * Update all addresses from local database to webservice
     */
    public function updateAll()
    {
        $addresses = $this->entityManager
            ->getRepository(Address::class)
            ->findAll();

        foreach ($addresses as $address) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'addresses',
                'id' => (int)$address->getIdAddress()
            ]);

            $addressFields = $xml->address->children();

            $addressFields->id_customer = $address->getIdCustomer();
            $addressFields->id_country = $address->getIdCountry();
            $addressFields->id_state = $address->getIdState();
            $addressFields->id_manufacturer = $address->getIdManufacturer();
            $addressFields->id_supplier = $address->getIdSupplier();
            $addressFields->id_warehouse = $address->getIdWarehouse();
            $addressFields->alias = $address->getAlias();
            $addressFields->company = $address->getCompany();
            $addressFields->lastname = $address->getLastname();
            $addressFields->firstname = $address->getFirstname();
            $addressFields->address1 = $address->getAddress1();
            $addressFields->address2 = $address->getAddress2();
            $addressFields->postcode = $address->getPostcode();
            $addressFields->city = $address->getCity();
            $addressFields->other = $address->getOther();
            $addressFields->phone = $address->getPhone();
            $addressFields->phone_mobile = $address->getPhoneMobile();
            $addressFields->vat_number = $address->getVatNumber();
            $addressFields->dni = $address->getDni();
            $addressFields->deleted = $address->getDeleted();
            sleep(1);

            $this->webService->edit([
                'resource' => 'addresses',
                'id' => (int)$addressFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}