<?php

namespace App\Services;


use App\Entity\Supplier;
use Doctrine\ORM\EntityManagerInterface;
use PrestaShopWebserviceException;

class SupplierService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Update webservice "suppliers" from local database
     *
     * @param $ids
     * @throws PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {

            sleep(1);

            $suppliers = $this->entityManager->getRepository(Supplier::class)->find($id);

            $xml = $this->webService->get([
                'resource' => 'suppliers',
                'id' => (int)$suppliers->getIdSupplier()
            ]);

            $supplierFields = $xml->supplier->children();

            $supplierFields->link_rewrite = $suppliers->getLinkRewrite();
            $supplierFields->name = $suppliers->getName();
            $supplierFields->active = $suppliers->getActive();
            $supplierFields->description = $suppliers->getDescription();
            $supplierFields->meta_title = $suppliers->getMetaTitle();
            $supplierFields->meta_description = $suppliers->getMetaDescription();
            $supplierFields->meta_keywords = $suppliers->getMetaKeywords();

            sleep(1);

            $this->webService->edit([
                'resource' => 'suppliers',
                'id' => (int)$supplierFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

    /**
     * Load data from webservice "suppliers" to local database
     */
    public function load()
    {
        $xml = $this->webService->get([
            'resource' => 'suppliers',
            'display' => 'full',
        ]);

        $suppliers = $xml->suppliers->children();

        foreach ($suppliers as $supplier) {
            $foundSupplier = $this->entityManager->getRepository(Supplier::class)
                ->findOneBy([
                    'id_supplier' => (int)$supplier->id
                ]);

            $newSupplier = $foundSupplier ? $foundSupplier : new Supplier;
            $newSupplier->setIdSupplier((int)$supplier->id);
            $newSupplier->setLinkRewrite($supplier->link_rewrite);
            $newSupplier->setName($supplier->name);
            $newSupplier->setActive((bool)(int)$supplier->active);
            $newSupplier->setDescription($supplier->description->language[0]);
            $newSupplier->setMetaTitle($supplier->meta_title->language[0]);
            $newSupplier->setMetaDescription($supplier->meta_description->language[0]);
            $newSupplier->setMetaKeywords($supplier->meta_keywords->language[0]);

            $this->entityManager->persist($newSupplier);
        }
        $this->entityManager->flush();
    }

    /**
     * Update all fields on webservice "suppliers" from local database
     */
    public function updateAll()
    {
        $suppliers = $this->entityManager
            ->getRepository(Supplier::class)
            ->findAll();

        foreach ($suppliers as $supplier) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'suppliers',
                'id' => (int)$supplier->getIdSupplier()
            ]);

            $supplierFields = $xml->supplier->children();
            $supplierFields->link_rewrite = $supplier->getLinkRewrite();
            $supplierFields->name = $supplier->getName();
            $supplierFields->active = $supplier->getActive();
            $supplierFields->description = $supplier->getDescription();
            $supplierFields->meta_title = $supplier->getMetaTitle();
            $supplierFields->meta_description = $supplier->getMetaDescription();
            $supplierFields->meta_keywords = $supplier->getMetaKeywords();

            sleep(1);

            $this->webService->edit([
                'resource' => 'suppliers',
                'id' => (int)$supplierFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }
}