<?php


namespace App\Services;


use App\Entity\Category;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use PrestaShopWebserviceException;
use Symfony\Component\HttpFoundation\Session\Session;

class CategoryService
{
    protected $entityManager;
    protected $prestashopService;
    protected $webService;

    public function __construct(EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService();
    }

    /**
     * Update webservice "categories" from local database
     * @param $ids
     * @throws PrestaShopWebserviceException
     */
    public function update($ids)
    {
        foreach ($ids as $id) {

            sleep(1);

            $category = $this->entityManager->getRepository(Category::class)->find($id);

            $xml = $this->webService->get([
                'resource' => 'categories',
                'id' => (int)$category->getIdCategory()
            ]);

            $categoryFields = $xml->category->children();

            $categoryFields->id_parent = $category->getIdParent();
            $categoryFields->id_shop_default = $category->getIdShopDefault();
            $categoryFields->active = (int)$category->getActive();
            $categoryFields->is_root_category = (int)$category->getIsRootCategory();

            $categoryFields->name->language[0] = $category->getName();

            unset($categoryFields->level_depth);
            unset($categoryFields->nb_products_recursive);

            sleep(1);

            $this->webService->edit([
                'resource' => 'categories',
                'id' => (int)$categoryFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }


    /**
     * Load data from webservice "categories" to local database
     */
    public function load()
    {
        $offset = 0;
        do {
            $xml = $this->webService->get([
                'resource' => 'categories',
                'display' => 'full',
                'limit' => "$offset, 1000",
            ]);

            $categories = $xml->categories->children();

            foreach ($categories as $category) {
                $foundCategory = $this->entityManager->getRepository(Category::class)
                    ->findOneBy([
                        'id_category' => (int)$category->id
                    ]);

                $newCategory = $foundCategory ? $foundCategory : new Category;

                $newCategory->setIdCategory((int)$category->id);
                $newCategory->setIdParent((int)$category->id_parent);
                $newCategory->setIdShopDefault((int)$category->id_shop_default);
                $newCategory->setActive((bool)(int)$category->active);
                $newCategory->setName($category->name->language[0]);
                $newCategory->setIsRootCategory((bool)(int)$category->is_root_category);
                $products = $category->associations->products->children();

                $productSession = new Session;

                foreach ($products as $product) {
                    /* @var Product $productEntity */
                    $productEntity = $this->entityManager->getRepository(Product::class)
                        ->findOneBy([
                            'id_product' => $product->id
                        ]);

                    if ($productEntity) {
                        $newCategory->addProduct($productEntity);
                    } else {
                        if (count($productSession->getFlashBag()->get('warning')) === 0) {
                            $productSession->getFlashBag()->add('warning', 'Please, add all products');
                        }
                        continue;
                    }

                }
                $this->entityManager->persist($newCategory);
            }

            $this->entityManager->flush();
            $offset += 1000;

        } while ($categories);
    }

    /**
     * Update all fields on webservice "categories" from local database
     */
    public function updateAll()
    {
        $categories = $this->entityManager
            ->getRepository(Category::class)
            ->findAll();

        foreach ($categories as $category) {

            sleep(1);

            $xml = $this->webService->get([
                'resource' => 'categories',
                'id' => (int)$category->getIdCategory()
            ]);

            $categoryFields = $xml->category->children();

            $categoryFields->id_parent = $category->getIdParent();
            $categoryFields->id_shop_default = $category->getIdShopDefault();
            $categoryFields->active = (int)$category->getActive();
            $categoryFields->name->language[0] = $category->getName();

            unset($categoryFields->level_depth);
            unset($categoryFields->nb_products_recursive);

            sleep(1);

            $this->webService->edit([
                'resource' => 'categories',
                'id' => (int)$categoryFields->id,
                'putXml' => $xml->asXML()
            ]);
        }
    }

}