<?php


namespace App\Command;

use App\Entity\PrestashopOrder;
use App\Entity\PrestashopOrderDetail;
use App\Entity\Product;
use App\Entity\ProductAttribute;
use App\Services\PrestashopService;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Swift_Attachment;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment;

class SendOrdersCommand extends Command
{
    protected static $defaultName = 'app:send-orders';

    protected $entityManager;
    protected $twig;
    protected $mailer;


    public function __construct(Environment $twig, EntityManagerInterface $em, Swift_Mailer $mailer)
    {
        parent::__construct();

        $this->twig = $twig;
        $this->entityManager = $em;
        $this->mailer = $mailer;
    }


    protected function configure()
    {
        $this
            ->setName('app:send-order')
            ->addArgument('supplier', InputArgument::REQUIRED, 'Supplier id')
            ->setDescription('Run to send a new orders...')
            ->setHelp('This command allows you to send new orders (created/updated during the last 1 day) to the supplier.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var PrestashopOrder $order */
        $order = $this->entityManager->getRepository(PrestashopOrder::class)->getSupplierTodaysOrder((int)$input->getArgument("supplier"));

        if (!$order) {
            $output->writeln("There no orders for this supplier today");
            return Command::FAILURE;
        }
        $image = 'https://www.alm-automation.fr/img/alm-automation-logo-1565242501.jpg';
        $type = pathinfo($image, PATHINFO_EXTENSION);

        $html = $this->twig->render("pdf/productEticket.html.twig", [
            'order' => $order,
            'image' => 'data:image/' . $type . ';base64,' . base64_encode(file_get_contents($image))
        ]);

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $dompdf = new Dompdf($pdfOptions);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $attachment = new Swift_Attachment($dompdf->output(), 'document.pdf', 'application/pdf');
        $message = (new Swift_Message($order->getReference() . 'Order'))
            ->setFrom('thephantommr@gmail.com')
            ->setTo($order->getSupplier()->getEmail())
            ->setBody("we need these products", 'text/plain')
            ->attach($attachment);
        $this->mailer->send($message);
        $order->setStatus(1);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
        $output->writeln('Order ' . $order->getReference() . ' are successfully sent to ' . $order->getSupplier()->getName());

        return Command::SUCCESS;
    }


}