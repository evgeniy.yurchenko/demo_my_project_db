<?php


namespace App\Command;

use App\Entity\PrestashopOrder;
use App\Entity\PrestashopOrderDetail;
use App\Entity\Product;
use App\Entity\ProductAttribute;
use App\Services\PrestashopService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Yaml\Yaml;

class CheckOrdersCommand extends Command
{
    protected static $defaultName = 'app:check-orders';

    protected $entityManager;
    protected $prestashopService;
    protected $webService;
    protected $parameterBag;


    public function __construct(ParameterBagInterface $parameterBag, EntityManagerInterface $em, PrestashopService $prestashopService)
    {
        parent::__construct();

        $this->parameterBag = $parameterBag;
        $this->entityManager = $em;
        $this->prestashopService = $prestashopService;
        $this->webService = $prestashopService->webService(false);
    }


    protected function configure()
    {
        $this
            ->setName('app:check-orders')
            // the short description shown while running "php bin/console list"
            ->setDescription('Run to check a new orders...')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to check new orders (created/updated during the last 1 hour).');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config_dir = $this->parameterBag->get("config.dir");
        $config_order = Yaml::parseFile($config_dir . '/prestashop_order.yaml');
        $order_search_ids = $this->parameterBag->get("CHECK_ORDER_IDS");
        $order_search_ids = (explode(",", $order_search_ids));
        $startDate = urlencode(date('Y-m-d H:i:s', time() - 3 * 3600));
        $lastDate = urlencode(date('Y-m-d H:i:s', time() + 3 * 3600));

        $output->writeln([
            'Checking for a new orders during last hour',
            '============',
            "[" . $startDate . ", " . $lastDate . "]",
            '============',
            '',
        ]);

        $productsForOrder = [];

        $xml = $this->webService->get([
            'resource' => 'orders?filter[date_upd]=' . "[" . $startDate . "," . $lastDate . "]&date=1&display=[id,current_state]",
        ]);

        $orders = $xml->orders->children();

        $ids_order = [];
        foreach ($orders as $order) {
//            if(in_array((int)$order["current_state"],$order_search_ids))
            $ids_order[] = (int)$order->id;
        }

        sleep(1);
        $xml = $this->webService->get([
            'resource' => 'order_details',
            'display' => '[product_id,product_attribute_id,id_order]',
            'filter[id_order]' => "[" . join("|", $ids_order) . "]"
        ]);

        $orderDetails = $xml->order_details->children();
        foreach ($orderDetails as $orderDetail) {
            $id_product = (int)$orderDetail->product_id;
            $id_product_attribute = (int)$orderDetail->product_attribute_id;
            $id_order = (int)$orderDetail->id_order;

            if ($id_product_attribute)
                $filter = ['filter[id_product_attribute]' => "[" . $id_product_attribute . "]"];
            else
                $filter = ['filter[id_product]' => "[" . $id_product . "]"];
            $params = [
                'resource' => 'stock_availables',
                'display' => '[quantity]',
            ];
            $params = array_merge($params, $filter);
            sleep(1);
            $xml = $this->webService->get($params);

            $stocks = $xml->stock_availables->children();
            foreach ($stocks as $stock) {
                if ((int)$stock->quantity < 0) {
                    $productsForOrder[] = [
                        "id_product" => $id_product,
                        "id_attribute" => $id_product_attribute,
                        "id_order" => $id_order,
                        "stock" => -(int)$stock->quantity
                    ];
                }
            }

        }
        foreach ($productsForOrder as $productForOrder) {

            /* @var Product $product */
            $product = $this->entityManager->getRepository(Product::class)->findOneBy([
                'id_product' => $productForOrder["id_product"]
            ]);
            /* @var ProductAttribute|null $productAttribute */
            $productAttribute = $this->entityManager->getRepository(ProductAttribute::class)->findOneBy([
                'id_product_attribute' => $productForOrder["id_attribute"]
            ]);

            $price = $productAttribute ? (float)$productAttribute->getWholesalePrice() : (float)$product->getWholesalePrice();

            /* @var PrestashopOrder|null $prestashopOrder */
            $prestashopOrder = $this->entityManager->getRepository(PrestashopOrder::class)->getSupplierTodaysOrder($product->getSupplier());
            $prestashopOrderDetail = null;
            if ($prestashopOrder) {
                /* @var PrestashopOrderDetail|null $prestashopOrderDetail */
                $prestashopOrderDetail = $this->entityManager->getRepository(PrestashopOrderDetail::class)->findOneBy([
                    "product" => $product->getId(),
                    "productAttribute" => $productAttribute ? $productAttribute->getId() : null,
                ]);
            } else {
                $prestashopOrder = new PrestashopOrder();
                $prestashopOrder->setDate(new \DateTime());
                $prestashopOrder->setSupplier($product->getSupplier());
                $year = substr(date("y"), -2);
                $month = date("m");
                $lastOrderId = (int)$config_order["order"]["last_id"];
                $prestashopOrder->setReference("CO" . $year . "-" . $month . sprintf("%02d", $lastOrderId));
                $config_order["order"]["last_id"]++;
                file_put_contents($config_dir . '/prestashop_order.yaml', Yaml::dump($config_order));
            }

            if (!$prestashopOrderDetail) {
                $prestashopOrderDetail = new PrestashopOrderDetail();
                $prestashopOrderDetail->setProductAttribute($productAttribute);
                $prestashopOrderDetail->setProduct($product);
                $prestashopOrder->addPrestashopOrderDetail($prestashopOrderDetail);
            }
            $array_orders = $prestashopOrderDetail->getOrders();
            if (!in_array($productForOrder["id_order"], $array_orders))
                array_push($array_orders, $productForOrder["id_order"]);
            $prestashopOrderDetail->setOrders($array_orders);
            $prestashopOrderDetail->setQuantity($productForOrder["stock"]);
            $prestashopOrderDetail->setPrice($price * $productForOrder["stock"]);

            $pricesSum = 0;
            foreach ($prestashopOrder->getPrestashopOrderDetails() as $detail) {
                $pricesSum += $detail->getPrice();
            }
            $prestashopOrder->setPrice($pricesSum);

            $this->entityManager->persist($prestashopOrderDetail);
            $this->entityManager->persist($prestashopOrder);
            $this->entityManager->flush();
        }

        // outputs a message without adding a "\n" at the end of the line
        $output->writeln('Order are successfully checked and loaded. ');

        return Command::SUCCESS;
    }


}