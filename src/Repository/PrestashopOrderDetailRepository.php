<?php

namespace App\Repository;

use App\Entity\PrestashopOrderDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PrestashopOrderDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrestashopOrderDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrestashopOrderDetail[]    findAll()
 * @method PrestashopOrderDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrestashopOrderDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrestashopOrderDetail::class);
    }

    // /**
    //  * @return PrestashopOrderDetail[] Returns an array of PrestashopOrderDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PrestashopOrderDetail
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
