<?php

namespace App\Repository;

use App\Entity\OfflineProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OfflineProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfflineProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfflineProduct[]    findAll()
 * @method OfflineProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfflineProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfflineProduct::class);
    }

    /**
     * @param $supplier
     * @param $value
     * @param $max
     * @return mixed
     */
    public function searchByNameStart($supplier, $value, $max): ?array
    {
        $value = "%" . $value . "%";
        return $this->createQueryBuilder('p')
            ->select("p.id, p.name, p.reference")
            ->where('p.supplier=:supplier AND (p.name LIKE :val OR p.reference LIKE :val)')
            ->setParameter('val', $value)
            ->setParameter('supplier', $supplier)
            ->orderBy('p.name', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return OfflineProduct[] Returns an array of OfflineProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfflineProduct
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
