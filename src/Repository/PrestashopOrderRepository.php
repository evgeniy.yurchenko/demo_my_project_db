<?php

namespace App\Repository;

use App\Entity\PrestashopOrder;
use App\Entity\Supplier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PrestashopOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrestashopOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrestashopOrder[]    findAll()
 * @method PrestashopOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrestashopOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrestashopOrder::class);
    }

    // /**
    //  * @return PrestashopOrder[] Returns an array of PrestashopOrder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    /**
     * @param Supplier|int $supplier
     * @return PrestashopOrder|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSupplierTodaysOrder($supplier): ?PrestashopOrder
    {
        $id = $supplier instanceof Supplier ? $supplier->getId() : (int)$supplier;
        return $this->createQueryBuilder('p')
            ->andWhere('p.date = :date AND p.supplier=:supplier AND p.status=0')
            ->setParameter('date', date('Y-m-d'))
            ->setParameter('supplier', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
