<?php

namespace App\Entity;

use App\Repository\PrestashopOrderDetailRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrestashopOrderDetailRepository::class)
 */
class PrestashopOrderDetail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=PrestashopOrder::class, inversedBy="prestashopOrderDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $prestashopOrder;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="prestashopOrderDetails")
     * @ORM\JoinColumn(nullable=true)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class, inversedBy="prestashopOrderDetails")
     */
    private $productAttribute;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity_validated;

    /**
     * @ORM\ManyToOne(targetEntity=OfflineProduct::class, inversedBy="prestashopOrderDetails")
     */
    private $offlineProduct;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $orders = [];


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrestashopOrder(): ?PrestashopOrder
    {
        return $this->prestashopOrder;
    }

    public function setPrestashopOrder(?PrestashopOrder $prestashopOrder): self
    {
        $this->prestashopOrder = $prestashopOrder;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductAttribute(): ?ProductAttribute
    {
        return $this->productAttribute;
    }

    public function setProductAttribute(?ProductAttribute $productAttribute): self
    {
        $this->productAttribute = $productAttribute;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?string
    {
        $product = $this->product ?
            $this->productAttribute ? $this->productAttribute : $this->product :
            $this->offlineProduct;

        $this->price = $product->getWholesalePrice() * $this->getQuantity();
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantityValidated(): ?int
    {
        return $this->quantity_validated;
    }

    public function setQuantityValidated(?int $quantity_validated): self
    {
        $this->quantity_validated = $quantity_validated;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getPrestashopOrder()->getReference();
    }

    public function getOfflineProduct(): ?OfflineProduct
    {
        return $this->offlineProduct;
    }

    public function setOfflineProduct(?OfflineProduct $offlineProduct): self
    {
        $this->offlineProduct = $offlineProduct;

        return $this;
    }

    public function getOrders(): ?array
    {
        return $this->orders;
    }

    public function setOrders(?array $orders): self
    {
        $this->orders = $orders;

        return $this;
    }
}
