<?php

namespace App\Entity;

use App\Repository\OrderDetailsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderDetailsRepository::class)
 */
class OrderDetails
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_order;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_order_detail;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_attribute_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $product_quantity_reinjected;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=2, nullable=true)
     */
    private $group_reduction;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $discount_quantity_applied;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_order_invoice;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_warehouse;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_shop;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_customization;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_quantity_in_stock;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_quantity_return;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_quantity_refunded;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $product_price;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=2)
     */
    private $reduction_percent;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $reduction_amount;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $reduction_amount_tax_incl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $reduction_amount_tax_excl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $product_quantity_discount;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $product_weight;

    /**
     * @ORM\Column(type="integer")
     */
    private $tax_computation_method;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_tax_rules_group;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=6)
     */
    private $ecotax;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=3)
     */
    private $ecotax_tax_rate;

    /**
     * @ORM\Column(type="integer")
     */
    private $download_nb;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $unit_price_tax_incl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $unit_price_tax_excl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $total_price_tax_incl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $total_price_tax_excl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $total_shipping_price_tax_excl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $total_shipping_price_tax_incl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $purchase_supplier_price;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $original_product_price;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $original_wholesale_price;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $connected_order;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="orderDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $connected_product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class, inversedBy="orderDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $connected_productAttribute;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdOrder(): ?int
    {
        return $this->id_order;
    }

    public function setIdOrder(int $id_order): self
    {
        $this->id_order = $id_order;

        return $this;
    }

    public function getIdOrderDetail(): ?int
    {
        return $this->id_order_detail;
    }

    public function setIdOrderDetail(int $id_order_detail): self
    {
        $this->id_order_detail = $id_order_detail;

        return $this;
    }

    public function getProductId(): ?int
    {
        return $this->product_id;
    }

    public function setProductId(int $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }

    public function getProductAttributeId(): ?int
    {
        return $this->product_attribute_id;
    }

    public function setProductAttributeId(int $product_attribute_id): self
    {
        $this->product_attribute_id = $product_attribute_id;

        return $this;
    }

    public function getProductQuantityReinjected(): ?int
    {
        return $this->product_quantity_reinjected;
    }

    public function setProductQuantityReinjected(?int $product_quantity_reinjected): self
    {
        $this->product_quantity_reinjected = $product_quantity_reinjected;

        return $this;
    }

    public function getGroupReduction(): ?string
    {
        return $this->group_reduction;
    }

    public function setGroupReduction(?string $group_reduction): self
    {
        $this->group_reduction = $group_reduction;

        return $this;
    }

    public function getDiscountQuantityApplied(): ?int
    {
        return $this->discount_quantity_applied;
    }

    public function setDiscountQuantityApplied(?int $discount_quantity_applied): self
    {
        $this->discount_quantity_applied = $discount_quantity_applied;

        return $this;
    }

    public function getIdOrderInvoice(): ?int
    {
        return $this->id_order_invoice;
    }

    public function setIdOrderInvoice(int $id_order_invoice): self
    {
        $this->id_order_invoice = $id_order_invoice;

        return $this;
    }

    public function getIdWarehouse(): ?int
    {
        return $this->id_warehouse;
    }

    public function setIdWarehouse(int $id_warehouse): self
    {
        $this->id_warehouse = $id_warehouse;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return $this->id_shop;
    }

    public function setIdShop(int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getIdCustomization(): ?int
    {
        return $this->id_customization;
    }

    public function setIdCustomization(int $id_customization): self
    {
        $this->id_customization = $id_customization;

        return $this;
    }

    public function getProductQuantity(): ?int
    {
        return $this->product_quantity;
    }

    public function setProductQuantity(int $product_quantity): self
    {
        $this->product_quantity = $product_quantity;

        return $this;
    }

    public function getProductQuantityInStock(): ?int
    {
        return $this->product_quantity_in_stock;
    }

    public function setProductQuantityInStock(int $product_quantity_in_stock): self
    {
        $this->product_quantity_in_stock = $product_quantity_in_stock;

        return $this;
    }

    public function getProductQuantityReturn(): ?int
    {
        return $this->product_quantity_return;
    }

    public function setProductQuantityReturn(int $product_quantity_return): self
    {
        $this->product_quantity_return = $product_quantity_return;

        return $this;
    }

    public function getProductQuantityRefunded(): ?int
    {
        return $this->product_quantity_refunded;
    }

    public function setProductQuantityRefunded(int $product_quantity_refunded): self
    {
        $this->product_quantity_refunded = $product_quantity_refunded;

        return $this;
    }

    public function getProductPrice(): ?string
    {
        return $this->product_price;
    }

    public function setProductPrice(string $product_price): self
    {
        $this->product_price = $product_price;

        return $this;
    }

    public function getReductionPercent(): ?string
    {
        return $this->reduction_percent;
    }

    public function setReductionPercent(string $reduction_percent): self
    {
        $this->reduction_percent = $reduction_percent;

        return $this;
    }

    public function getReductionAmount(): ?string
    {
        return $this->reduction_amount;
    }

    public function setReductionAmount(string $reduction_amount): self
    {
        $this->reduction_amount = $reduction_amount;

        return $this;
    }

    public function getReductionAmountTaxIncl(): ?string
    {
        return $this->reduction_amount_tax_incl;
    }

    public function setReductionAmountTaxIncl(string $reduction_amount_tax_incl): self
    {
        $this->reduction_amount_tax_incl = $reduction_amount_tax_incl;

        return $this;
    }

    public function getReductionAmountTaxExcl(): ?string
    {
        return $this->reduction_amount_tax_excl;
    }

    public function setReductionAmountTaxExcl(string $reduction_amount_tax_excl): self
    {
        $this->reduction_amount_tax_excl = $reduction_amount_tax_excl;

        return $this;
    }

    public function getProductQuantityDiscount(): ?string
    {
        return $this->product_quantity_discount;
    }

    public function setProductQuantityDiscount(string $product_quantity_discount): self
    {
        $this->product_quantity_discount = $product_quantity_discount;

        return $this;
    }

    public function getProductWeight(): ?string
    {
        return $this->product_weight;
    }

    public function setProductWeight(string $product_weight): self
    {
        $this->product_weight = $product_weight;

        return $this;
    }

    public function getTaxComputationMethod(): ?int
    {
        return $this->tax_computation_method;
    }

    public function setTaxComputationMethod(int $tax_computation_method): self
    {
        $this->tax_computation_method = $tax_computation_method;

        return $this;
    }

    public function getIdTaxRulesGroup(): ?int
    {
        return $this->id_tax_rules_group;
    }

    public function setIdTaxRulesGroup(int $id_tax_rules_group): self
    {
        $this->id_tax_rules_group = $id_tax_rules_group;

        return $this;
    }

    public function getEcotax(): ?string
    {
        return $this->ecotax;
    }

    public function setEcotax(string $ecotax): self
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    public function getEcotaxTaxRate(): ?string
    {
        return $this->ecotax_tax_rate;
    }

    public function setEcotaxTaxRate(string $ecotax_tax_rate): self
    {
        $this->ecotax_tax_rate = $ecotax_tax_rate;

        return $this;
    }

    public function getDownloadNb(): ?int
    {
        return $this->download_nb;
    }

    public function setDownloadNb(int $download_nb): self
    {
        $this->download_nb = $download_nb;

        return $this;
    }

    public function getUnitPriceTaxIncl(): ?string
    {
        return $this->unit_price_tax_incl;
    }

    public function setUnitPriceTaxIncl(string $unit_price_tax_incl): self
    {
        $this->unit_price_tax_incl = $unit_price_tax_incl;

        return $this;
    }

    public function getUnitPriceTaxExcl(): ?string
    {
        return $this->unit_price_tax_excl;
    }

    public function setUnitPriceTaxExcl(string $unit_price_tax_excl): self
    {
        $this->unit_price_tax_excl = $unit_price_tax_excl;

        return $this;
    }

    public function getTotalPriceTaxIncl(): ?string
    {
        return $this->total_price_tax_incl;
    }

    public function setTotalPriceTaxIncl(string $total_price_tax_incl): self
    {
        $this->total_price_tax_incl = $total_price_tax_incl;

        return $this;
    }

    public function getTotalPriceTaxExcl(): ?string
    {
        return $this->total_price_tax_excl;
    }

    public function setTotalPriceTaxExcl(string $total_price_tax_excl): self
    {
        $this->total_price_tax_excl = $total_price_tax_excl;

        return $this;
    }

    public function getTotalShippingPriceTaxExcl(): ?string
    {
        return $this->total_shipping_price_tax_excl;
    }

    public function setTotalShippingPriceTaxExcl(string $total_shipping_price_tax_excl): self
    {
        $this->total_shipping_price_tax_excl = $total_shipping_price_tax_excl;

        return $this;
    }

    public function getTotalShippingPriceTaxIncl(): ?string
    {
        return $this->total_shipping_price_tax_incl;
    }

    public function setTotalShippingPriceTaxIncl(string $total_shipping_price_tax_incl): self
    {
        $this->total_shipping_price_tax_incl = $total_shipping_price_tax_incl;

        return $this;
    }

    public function getPurchaseSupplierPrice(): ?string
    {
        return $this->purchase_supplier_price;
    }

    public function setPurchaseSupplierPrice(string $purchase_supplier_price): self
    {
        $this->purchase_supplier_price = $purchase_supplier_price;

        return $this;
    }

    public function getOriginalProductPrice(): ?string
    {
        return $this->original_product_price;
    }

    public function setOriginalProductPrice(string $original_product_price): self
    {
        $this->original_product_price = $original_product_price;

        return $this;
    }

    public function getOriginalWholesalePrice(): ?string
    {
        return $this->original_wholesale_price;
    }

    public function setOriginalWholesalePrice(string $original_wholesale_price): self
    {
        $this->original_wholesale_price = $original_wholesale_price;

        return $this;
    }

    public function getConnectedOrder(): ?Order
    {
        return $this->connected_order;
    }

    public function setConnectedOrder(?Order $connected_order): self
    {
        $this->connected_order = $connected_order;

        return $this;
    }

    public function getConnectedProduct(): ?Product
    {
        return $this->connected_product;
    }

    public function setConnectedProduct(?Product $connected_product): self
    {
        $this->connected_product = $connected_product;

        return $this;
    }

    public function getConnectedProductAttribute(): ?ProductAttribute
    {
        return $this->connected_productAttribute;
    }

    public function setConnectedProductAttribute(?ProductAttribute $connected_productAttribute): self
    {
        $this->connected_productAttribute = $connected_productAttribute;

        return $this;
    }
}
