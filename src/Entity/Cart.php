<?php

namespace App\Entity;

use App\Repository\CartRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CartRepository::class)
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_cart;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_shop_group;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_shop;

    /**
     * @ORM\ManyToOne(targetEntity=Carrier::class, inversedBy="carts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $carrier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $delivery_option;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_lang;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="carts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cusomer;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_guest;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secure_key;

    /**
     * @ORM\Column(type="boolean")
     */
    private $recyclable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gift;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gift_message;

    /**
     * @ORM\Column(type="date")
     */
    private $date_add;

    /**
     * @ORM\Column(type="date")
     */
    private $date_upd;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="cart")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity=CartProduct::class, mappedBy="cart")
     */
    private $cartProducts;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="carts_delivery")
     */
    private $address_delivery;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="carts_invoice")
     */
    private $address_invoice;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->cartProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCart(): ?int
    {
        return $this->id_cart;
    }

    public function setIdCart(int $id_cart): self
    {
        $this->id_cart = $id_cart;

        return $this;
    }

    public function getIdShopGroup(): ?int
    {
        return $this->id_shop_group;
    }

    public function setIdShopGroup(int $id_shop_group): self
    {
        $this->id_shop_group = $id_shop_group;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return $this->id_shop;
    }

    public function setIdShop(int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getCarrier(): ?Carrier
    {
        return $this->carrier;
    }

    public function setCarrier(?Carrier $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getDeliveryOption(): ?string
    {
        return $this->delivery_option;
    }

    public function setDeliveryOption(?string $delivery_option): self
    {
        $this->delivery_option = $delivery_option;

        return $this;
    }

    public function getIdLang(): ?int
    {
        return $this->id_lang;
    }

    public function setIdLang(int $id_lang): self
    {
        $this->id_lang = $id_lang;

        return $this;
    }

    public function getCusomer(): ?Customer
    {
        return $this->cusomer;
    }

    public function setCusomer(?Customer $cusomer): self
    {
        $this->cusomer = $cusomer;

        return $this;
    }

    public function getIdGuest(): ?int
    {
        return $this->id_guest;
    }

    public function setIdGuest(int $id_guest): self
    {
        $this->id_guest = $id_guest;

        return $this;
    }

    public function getSecureKey(): ?string
    {
        return $this->secure_key;
    }

    public function setSecureKey(string $secure_key): self
    {
        $this->secure_key = $secure_key;

        return $this;
    }

    public function getRecyclable(): ?bool
    {
        return $this->recyclable;
    }

    public function setRecyclable(bool $recyclable): self
    {
        $this->recyclable = $recyclable;

        return $this;
    }

    public function getGift(): ?bool
    {
        return $this->gift;
    }

    public function setGift(bool $gift): self
    {
        $this->gift = $gift;

        return $this;
    }

    public function getGiftMessage(): ?string
    {
        return $this->gift_message;
    }

    public function setGiftMessage(?string $gift_message): self
    {
        $this->gift_message = $gift_message;

        return $this;
    }

    public function getDateAdd(): ?DateTimeInterface
    {
        return $this->date_add;
    }

    public function setDateAdd(DateTimeInterface $date_add): self
    {
        $this->date_add = $date_add;

        return $this;
    }

    public function getDateUpd(): ?DateTimeInterface
    {
        return $this->date_upd;
    }

    public function setDateUpd(DateTimeInterface $date_upd): self
    {
        $this->date_upd = $date_upd;

        return $this;
    }


    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->orders->first();
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCart($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getCart() === $this) {
                $order->setCart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CartProduct[]
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): self
    {
        if (!$this->cartProducts->contains($cartProduct)) {
            $this->cartProducts[] = $cartProduct;
            $cartProduct->setCart($this);
        }

        return $this;
    }

    public function removeCartProduct(CartProduct $cartProduct): self
    {
        if ($this->cartProducts->contains($cartProduct)) {
            $this->cartProducts->removeElement($cartProduct);
            // set the owning side to null (unless already changed)
            if ($cartProduct->getCart() === $this) {
                $cartProduct->setCart(null);
            }
        }

        return $this;
    }

    public function getAddressDelivery(): ?Address
    {
        return $this->address_delivery;
    }

    public function setAddressDelivery(?Address $address_delivery): self
    {
        $this->address_delivery = $address_delivery;

        return $this;
    }

    public function getAddressInvoice(): ?Address
    {
        return $this->address_invoice;
    }

    public function setAddressInvoice(?Address $address_invoice): self
    {
        $this->address_invoice = $address_invoice;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getDeliveryOption();
    }
}
