<?php

namespace App\Entity;

use App\Repository\ProductAttributeRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductAttributeRepository::class)
 */
class ProductAttribute
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product_attribute;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $supplier_reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ean13;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $upc;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $wholesale_price;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $weight;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $unit_price_impact;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $default_on;

    /**
     * @ORM\Column(type="integer")
     */
    private $minimal_quantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $low_stock_threshold;

    /**
     * @ORM\Column(type="boolean")
     */
    private $low_stock_alert;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $available_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_upd;

    /**
     * @ORM\Column(type="decimal", precision=17, scale=6)
     */
    private $ecotax;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productAttributes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\OneToMany(targetEntity=CartProduct::class, mappedBy="productAttribute")
     */
    private $cartProducts;

    /**
     * @ORM\OneToMany(targetEntity=OrderDetails::class, mappedBy="connected_productAttribute")
     */
    private $orderDetails;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="product_attribute")
     */
    private $stocks;

    /**
     * @ORM\OneToMany(targetEntity=SupplierOrderDetails::class, mappedBy="product_attribute")
     */
    private $supplierOrderDetails;

    /**
     * @ORM\OneToMany(targetEntity=PrestashopOrderDetail::class, mappedBy="productAttribute")
     */
    private $prestashopOrderDetails;

    public function __construct()
    {
        $this->cartProducts = new ArrayCollection();
        $this->orderDetails = new ArrayCollection();
        $this->stocks = new ArrayCollection();
        $this->supplierOrderDetails = new ArrayCollection();
        $this->prestashopOrderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdProductAttribute(): ?int
    {
        return $this->id_product_attribute;
    }

    public function setIdProductAttribute(int $id_product_attribute): self
    {
        $this->id_product_attribute = $id_product_attribute;

        return $this;
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSupplierReference(): ?string
    {
        return $this->supplier_reference;
    }

    public function setSupplierReference(?string $supplier_reference): self
    {
        $this->supplier_reference = $supplier_reference;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(?string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getUpc(): ?string
    {
        return $this->upc;
    }

    public function setUpc(?string $upc): self
    {
        $this->upc = $upc;

        return $this;
    }

    public function getWholesalePrice(): ?string
    {
        return $this->wholesale_price;
    }

    public function setWholesalePrice(string $wholesale_price): self
    {
        $this->wholesale_price = $wholesale_price;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getUnitPriceImpact(): ?string
    {
        return $this->unit_price_impact;
    }

    public function setUnitPriceImpact(string $unit_price_impact): self
    {
        $this->unit_price_impact = $unit_price_impact;

        return $this;
    }

    public function getDefaultOn(): ?bool
    {
        return $this->default_on;
    }

    public function setDefaultOn(?bool $default_on): self
    {
        $this->default_on = $default_on;

        return $this;
    }

    public function getMinimalQuantity(): ?int
    {
        return $this->minimal_quantity;
    }

    public function setMinimalQuantity(int $minimal_quantity): self
    {
        $this->minimal_quantity = $minimal_quantity;

        return $this;
    }

    public function getLowStockThreshold(): ?int
    {
        return $this->low_stock_threshold;
    }

    public function setLowStockThreshold(?int $low_stock_threshold): self
    {
        $this->low_stock_threshold = $low_stock_threshold;

        return $this;
    }

    public function getLowStockAlert(): ?bool
    {
        return $this->low_stock_alert;
    }

    public function setLowStockAlert(bool $low_stock_alert): self
    {
        $this->low_stock_alert = $low_stock_alert;

        return $this;
    }

    public function getAvailableDate(): ?DateTimeInterface
    {
        return $this->available_date;
    }

    public function setAvailableDate(?DateTimeInterface $available_date): self
    {
        $this->available_date = $available_date;

        return $this;
    }

    public function getDateUpd(): ?DateTimeInterface
    {
        return $this->date_upd;
    }

    public function setDateUpd(?DateTimeInterface $date_upd): self
    {
        $this->date_upd = $date_upd;

        return $this;
    }

    public function getEcotax(): ?string
    {
        return $this->ecotax;
    }

    public function setEcotax(string $ecotax): self
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function __toString()
    {
        return $this->reference;
    }

    /**
     * @return Collection|CartProduct[]
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): self
    {
        if (!$this->cartProducts->contains($cartProduct)) {
            $this->cartProducts[] = $cartProduct;
            $cartProduct->setProductAttribute($this);
        }

        return $this;
    }

    public function removeCartProduct(CartProduct $cartProduct): self
    {
        if ($this->cartProducts->contains($cartProduct)) {
            $this->cartProducts->removeElement($cartProduct);
            // set the owning side to null (unless already changed)
            if ($cartProduct->getProductAttribute() === $this) {
                $cartProduct->setProductAttribute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderDetails[]
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function addOrderDetail(OrderDetails $orderDetail): self
    {
        if (!$this->orderDetails->contains($orderDetail)) {
            $this->orderDetails[] = $orderDetail;
            $orderDetail->setConnectedProductAttribute($this);
        }

        return $this;
    }

    public function removeOrderDetail(OrderDetails $orderDetail): self
    {
        if ($this->orderDetails->contains($orderDetail)) {
            $this->orderDetails->removeElement($orderDetail);
            // set the owning side to null (unless already changed)
            if ($orderDetail->getConnectedProductAttribute() === $this) {
                $orderDetail->setConnectedProductAttribute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setProductAttribute($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->contains($stock)) {
            $this->stocks->removeElement($stock);
            // set the owning side to null (unless already changed)
            if ($stock->getProductAttribute() === $this) {
                $stock->setProductAttribute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SupplierOrderDetails[]
     */
    public function getSupplierOrderDetails(): Collection
    {
        return $this->supplierOrderDetails;
    }

    public function addSupplierOrderDetail(SupplierOrderDetails $supplierOrderDetail): self
    {
        if (!$this->supplierOrderDetails->contains($supplierOrderDetail)) {
            $this->supplierOrderDetails[] = $supplierOrderDetail;
            $supplierOrderDetail->setProductAttribute($this);
        }

        return $this;
    }

    public function removeSupplierOrderDetail(SupplierOrderDetails $supplierOrderDetail): self
    {
        if ($this->supplierOrderDetails->contains($supplierOrderDetail)) {
            $this->supplierOrderDetails->removeElement($supplierOrderDetail);
            // set the owning side to null (unless already changed)
            if ($supplierOrderDetail->getProductAttribute() === $this) {
                $supplierOrderDetail->setProductAttribute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PrestashopOrderDetail[]
     */
    public function getPrestashopOrderDetails(): Collection
    {
        return $this->prestashopOrderDetails;
    }

    public function addPrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if (!$this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails[] = $prestashopOrderDetail;
            $prestashopOrderDetail->setProductAttribute($this);
        }

        return $this;
    }

    public function removePrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if ($this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails->removeElement($prestashopOrderDetail);
            // set the owning side to null (unless already changed)
            if ($prestashopOrderDetail->getProductAttribute() === $this) {
                $prestashopOrderDetail->setProductAttribute(null);
            }
        }

        return $this;
    }
}
