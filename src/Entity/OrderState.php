<?php

namespace App\Entity;

use App\Repository\OrderStateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderStateRepository::class)
 */
class OrderState
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_state;

    /**
     * @ORM\Column(type="boolean")
     */
    private $unremovable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $delivery;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @ORM\Column(type="boolean")
     */
    private $send_email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $invoice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="boolean")
     */
    private $logable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $shipped;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paid;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pdf_delivery;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pdf_invoice;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deleted;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="state")
     */
    private $orders;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isForPreparation = false;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getIdState(): ?int
    {
        return $this->id_state;
    }

    public function setIdState(int $id_state): self
    {
        $this->id_state = $id_state;

        return $this;
    }

    public function getUnremovable(): ?bool
    {
        return $this->unremovable;
    }

    public function setUnremovable(bool $unremovable): self
    {
        $this->unremovable = $unremovable;

        return $this;
    }

    public function getDelivery(): ?bool
    {
        return $this->delivery;
    }

    public function setDelivery(bool $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }

    public function getHidden(): ?bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): self
    {
        $this->hidden = $hidden;

        return $this;
    }

    public function getSendEmail(): ?bool
    {
        return $this->send_email;
    }

    public function setSendEmail(bool $send_email): self
    {
        $this->send_email = $send_email;

        return $this;
    }

    public function getInvoice(): ?bool
    {
        return $this->invoice;
    }

    public function setInvoice(?bool $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getLogable(): ?bool
    {
        return $this->logable;
    }

    public function setLogable(bool $logable): self
    {
        $this->logable = $logable;

        return $this;
    }

    public function getShipped(): ?bool
    {
        return $this->shipped;
    }

    public function setShipped(bool $shipped): self
    {
        $this->shipped = $shipped;

        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getPdfDelivery(): ?bool
    {
        return $this->pdf_delivery;
    }

    public function setPdfDelivery(bool $pdf_delivery): self
    {
        $this->pdf_delivery = $pdf_delivery;

        return $this;
    }

    public function getPdfInvoice(): ?bool
    {
        return $this->pdf_invoice;
    }

    public function setPdfInvoice(bool $pdf_invoice): self
    {
        $this->pdf_invoice = $pdf_invoice;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setState($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getState() === $this) {
                $order->setState(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    public function getIsForPreparation(): ?bool
    {
        return $this->isForPreparation;
    }

    public function setIsForPreparation(bool $isForPreparation): self
    {
        $this->isForPreparation = $isForPreparation;

        return $this;
    }
}
