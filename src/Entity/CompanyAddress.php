<?php

namespace App\Entity;

use App\Repository\CompanyAddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyAddressRepository::class)
 */
class CompanyAddress
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vat_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $other;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_mobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dni;

    /**
     * @ORM\OneToMany(targetEntity=PrestashopOrder::class, mappedBy="companyAddress", cascade={"remove"})
     */
    private $prestashop_orders;

    /**
     * @ORM\OneToMany(targetEntity=PrestashopOrder::class, mappedBy="invoice_address")
     */
    private $invoice_prestashop_orders;

    public function __construct()
    {
        $this->prestashop_orders = new ArrayCollection();
        $this->invoice_prestashop_orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getVatNumber(): ?string
    {
        return $this->vat_number;
    }

    public function setVatNumber(?string $vat_number): self
    {
        $this->vat_number = $vat_number;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(?string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(?string $other): self
    {
        $this->other = $other;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneMobile(): ?string
    {
        return $this->phone_mobile;
    }

    public function setPhoneMobile(?string $phone_mobile): self
    {
        $this->phone_mobile = $phone_mobile;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(?string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }


    public function __toString()
    {
        return (string)$this->dni;
    }

    /**
     * @return Collection|PrestashopOrder[]
     */
    public function getPrestashopOrders(): Collection
    {
        return $this->prestashop_orders;
    }

    public function addPrestashopOrder(PrestashopOrder $prestashopOrder): self
    {
        if (!$this->prestashop_orders->contains($prestashopOrder)) {
            $this->prestashop_orders[] = $prestashopOrder;
            $prestashopOrder->setCompanyAddress($this);
        }

        return $this;
    }

    public function removePrestashopOrder(PrestashopOrder $prestashopOrder): self
    {
        if ($this->prestashop_orders->contains($prestashopOrder)) {
            $this->prestashop_orders->removeElement($prestashopOrder);
            // set the owning side to null (unless already changed)
            if ($prestashopOrder->getCompanyAddress() === $this) {
                $prestashopOrder->setCompanyAddress(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PrestashopOrder[]
     */
    public function getInvoicePrestashopOrders(): Collection
    {
        return $this->invoice_prestashop_orders;
    }

    public function addInvoicePrestashopOrder(PrestashopOrder $prestashopOrder): self
    {
        if (!$this->invoice_prestashop_orders->contains($prestashopOrder)) {
            $this->invoice_prestashop_orders[] = $prestashopOrder;
            $prestashopOrder->setCompanyAddress($this);
        }

        return $this;
    }

    public function removeInvoicePrestashopOrder(PrestashopOrder $prestashopOrder): self
    {
        if ($this->invoice_prestashop_orders->contains($prestashopOrder)) {
            $this->invoice_prestashop_orders->removeElement($prestashopOrder);
            // set the owning side to null (unless already changed)
            if ($prestashopOrder->getInvoiceAddress() === $this) {
                $prestashopOrder->setInvoiceAddress(null);
            }
        }

        return $this;
    }
}
