<?php

namespace App\Entity;

use App\Repository\CartProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CartProductRepository::class)
 */
class CartProduct
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Cart::class, inversedBy="cartProducts")
     */
    private $cart;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="cartProducts")
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_address_delivery;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="cartProducts")
     */
    private $address_delivery;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_shop;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_customization;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class, inversedBy="cartProducts")
     */
    private $productAttribute;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_cart;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product_attribute;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getIdAddressDelivery(): ?int
    {
        return $this->id_address_delivery;
    }

    public function setIdAddressDelivery(int $id_address_delivery): self
    {
        $this->id_address_delivery = $id_address_delivery;

        return $this;
    }

    public function getAddressDelivery(): ?Address
    {
        return $this->address_delivery;
    }

    public function setAddressDelivery(?Address $address_delivery): self
    {
        $this->address_delivery = $address_delivery;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return $this->id_shop;
    }

    public function setIdShop(int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getIdCustomization(): ?int
    {
        return $this->id_customization;
    }

    public function setIdCustomization(int $id_customization): self
    {
        $this->id_customization = $id_customization;

        return $this;
    }

    public function getProductAttribute(): ?ProductAttribute
    {
        return $this->productAttribute;
    }

    public function setProductAttribute(?ProductAttribute $productAttribute): self
    {
        $this->productAttribute = $productAttribute;

        return $this;
    }

    public function getIdCart(): ?int
    {
        return $this->id_cart;
    }

    public function setIdCart(int $id_cart): self
    {
        $this->id_cart = $id_cart;

        return $this;
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getIdProductAttribute(): ?int
    {
        return $this->id_product_attribute;
    }

    public function setIdProductAttribute(int $id_product_attribute): self
    {
        $this->id_product_attribute = $id_product_attribute;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getProduct()->getName();
    }
}
