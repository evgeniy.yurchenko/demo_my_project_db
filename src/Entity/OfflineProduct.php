<?php

namespace App\Entity;

use App\Repository\OfflineProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfflineProductRepository::class)
 */
class OfflineProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="offlineProducts")
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ean13;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $upc;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $wholesale_price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $supplier_reference;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6, nullable=true)
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="offlineProducts")
     */
    private $supplier;


    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="offlineProducts")
     */
    private $manufacturer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=PrestashopOrderDetail::class, mappedBy="offlineProduct")
     */
    private $prestashopOrderDetails;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->prestashopOrderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getUpc(): ?string
    {
        return $this->upc;
    }

    public function setUpc(?string $upc): self
    {
        $this->upc = $upc;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getWholesalePrice(): ?string
    {
        return $this->wholesale_price;
    }

    public function setWholesalePrice(string $wholesale_price): self
    {
        $this->wholesale_price = $wholesale_price;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSupplierReference(): ?string
    {
        return $this->supplier_reference;
    }

    public function setSupplierReference(string $supplier_reference): self
    {
        $this->supplier_reference = $supplier_reference;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(?string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }


    public function getManufacturer(): ?Brand
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Brand $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|PrestashopOrderDetail[]
     */
    public function getPrestashopOrderDetails(): Collection
    {
        return $this->prestashopOrderDetails;
    }

    public function addPrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if (!$this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails[] = $prestashopOrderDetail;
            $prestashopOrderDetail->setOfflineProduct($this);
        }

        return $this;
    }

    public function removePrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if ($this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails->removeElement($prestashopOrderDetail);
            // set the owning side to null (unless already changed)
            if ($prestashopOrderDetail->getOfflineProduct() === $this) {
                $prestashopOrderDetail->setOfflineProduct(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getReference();
    }

    public function encodeImageToBase64()
    {
        return null;
    }
}
