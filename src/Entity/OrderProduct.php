<?php

namespace App\Entity;

use App\Repository\OrderProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderProductRepository::class)
 */
class OrderProduct
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_order_product;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_product_attribute;
    /**
     * @ORM\Column(type="integer")
     */
    private $product_quantity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $product_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $product_reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $product_ean13;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $product_isbn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $product_upc;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $product_price;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $unit_price_tax_incl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $unit_price_tax_excl;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderConnected;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="orderProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class)
     */
    private $productAttribute;

    /**
     * @ORM\Column(type="integer")
     */
    private $order_id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_validated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdOrderProduct(): ?int
    {
        return $this->id_order_product;
    }

    public function setIdOrderProduct(int $id_order_product): self
    {
        $this->id_order_product = $id_order_product;

        return $this;
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getIdProductattribute(): ?int
    {
        return $this->id_product_attribute;
    }

    public function setIdProductattribute(int $id_product_attribute): self
    {
        $this->id_product_attribute = $id_product_attribute;

        return $this;
    }

    public function getProductQuantity(): ?int
    {
        return $this->product_quantity;
    }

    public function setProductQuantity(int $product_quantity): self
    {
        $this->product_quantity = $product_quantity;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->product_name;
    }

    public function setProductName(string $product_name): self
    {
        $this->product_name = $product_name;

        return $this;
    }

    public function getProductReference(): ?string
    {
        return $this->product_reference;
    }

    public function setProductReference(string $product_reference): self
    {
        $this->product_reference = $product_reference;

        return $this;
    }

    public function getProductEan13(): ?string
    {
        return $this->product_ean13;
    }

    public function setProductEan13(string $product_ean13): self
    {
        $this->product_ean13 = $product_ean13;

        return $this;
    }

    public function getProductIsbn(): ?string
    {
        return $this->product_isbn;
    }

    public function setProductIsbn(?string $product_isbn): self
    {
        $this->product_isbn = $product_isbn;

        return $this;
    }

    public function getProductUpc(): ?string
    {
        return $this->product_upc;
    }

    public function setProductUpc(?string $product_upc): self
    {
        $this->product_upc = $product_upc;

        return $this;
    }

    public function getProductPrice(): ?string
    {
        return $this->product_price;
    }

    public function setProductPrice(string $product_price): self
    {
        $this->product_price = $product_price;

        return $this;
    }

    public function getUnitPriceTaxIncl(): ?string
    {
        return $this->unit_price_tax_incl;
    }

    public function setUnitPriceTaxIncl(string $unit_price_tax_incl): self
    {
        $this->unit_price_tax_incl = $unit_price_tax_incl;

        return $this;
    }

    public function getUnitPriceTaxExcl(): ?string
    {
        return $this->unit_price_tax_excl;
    }

    public function setUnitPriceTaxExcl(string $unit_price_tax_excl): self
    {
        $this->unit_price_tax_excl = $unit_price_tax_excl;

        return $this;
    }

    public function getOrderConnected(): ?Order
    {
        return $this->orderConnected;
    }

    public function setOrderConnected(?Order $orderConnected): self
    {
        $this->orderConnected = $orderConnected;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductAttribute(): ?ProductAttribute
    {
        return $this->productAttribute;
    }

    public function setProductAttribute(?ProductAttribute $productAttribute): self
    {
        $this->productAttribute = $productAttribute;

        return $this;
    }

    public function getOrderId(): ?int
    {
        return $this->order_id;
    }

    public function setOrderId(int $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->product_name;
    }

    public function getIsValidated(): ?bool
    {
        if (is_null($this->is_validated)) {
            $stock = $this->getProduct()->getAvailableStock($this->getProductAttribute());
            return $stock >= 0;
        }
        return $this->is_validated;
    }

    public function setIsValidated(?bool $is_validated): self
    {
        $this->is_validated = $is_validated;

        return $this;
    }
}
