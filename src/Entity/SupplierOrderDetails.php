<?php

namespace App\Entity;

use App\Repository\SupplierOrderDetailsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SupplierOrderDetailsRepository::class)
 */
class SupplierOrderDetails
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_supplier_order_details;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product_attribute;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="supplierOrderDetails")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class, inversedBy="supplierOrderDetails")
     */
    private $product_attribute;

    /**
     * @ORM\ManyToOne(targetEntity=SupplierOrder::class, inversedBy="supplierOrderDetails", cascade={"persist", "remove"}))
     * @ORM\JoinColumn(nullable=true)
     */
    private $supply_order;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdSupplierOrderDetails(): ?int
    {
        return $this->id_supplier_order_details;
    }

    public function setIdSupplierOrderDetails(int $id_supplier_order_details): self
    {
        $this->id_supplier_order_details = $id_supplier_order_details;

        return $this;
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getIdProductAttribute(): ?int
    {
        return $this->id_product_attribute;
    }

    public function setIdProductAttribute(int $id_product_attribute): self
    {
        $this->id_product_attribute = $id_product_attribute;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;
        $this->setIdProduct($product->getIdProduct());

        return $this;
    }

    public function getProductAttribute(): ?ProductAttribute
    {
        return $this->product_attribute;
    }

    public function setProductAttribute(?ProductAttribute $product_attribute): self
    {
        $this->product_attribute = $product_attribute;
        $this->setIdProductAttribute($product_attribute->getIdProductAttribute());

        return $this;
    }

    public function getSupplyOrder(): ?SupplierOrder
    {
        return $this->supply_order;
    }

    public function setSupplyOrder(?SupplierOrder $supply_order): self
    {
        $this->supply_order = $supply_order;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getProduct()->getName();
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
