<?php

namespace App\Entity;

use App\Repository\SupplierOrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SupplierOrderRepository::class)
 */
class SupplierOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_supply_order;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_supplier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_warehouse;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_supply_order_state;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_currency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $supplier_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_delivery_expected;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=6)
     */
    private $total_te;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=6)
     */
    private $total_with_discount_te;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=6)
     */
    private $total_ti;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=6)
     */
    private $total_tax;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=6, nullable=true)
     */
    private $discount_rate;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=6)
     */
    private $discount_value_te;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_template;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_add;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_upd;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="supplierOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Supplier;

    /**
     * @ORM\OneToMany(targetEntity=SupplierOrderDetails::class, mappedBy="supply_order", cascade={"persist", "remove"})
     */
    private $supplierOrderDetails;

    public function __construct()
    {
        $this->supplierOrderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdSupplyOrder(): ?int
    {
        return $this->id_supply_order;
    }

    public function setIdSupplyOrder(int $id_supply_order): self
    {
        $this->id_supply_order = $id_supply_order;

        return $this;
    }

    public function getIdSupplier(): ?int
    {
        return $this->id_supplier;
    }

    public function setIdSupplier(int $id_supplier): self
    {
        $this->id_supplier = $id_supplier;

        return $this;
    }

    public function getIdWarehouse(): ?int
    {
        return $this->id_warehouse;
    }

    public function setIdWarehouse(?int $id_warehouse): self
    {
        $this->id_warehouse = $id_warehouse;

        return $this;
    }

    public function getIdSupplyOrderState(): ?int
    {
        return $this->id_supply_order_state;
    }

    public function setIdSupplyOrderState(int $id_supply_order_state): self
    {
        $this->id_supply_order_state = $id_supply_order_state;

        return $this;
    }

    public function getIdCurrency(): ?int
    {
        return $this->id_currency;
    }

    public function setIdCurrency(int $id_currency): self
    {
        $this->id_currency = $id_currency;

        return $this;
    }

    public function getSupplierName(): ?string
    {
        return $this->supplier_name;
    }

    public function setSupplierName(?string $supplier_name): self
    {
        $this->supplier_name = $supplier_name;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDateDeliveryExpected(): ?\DateTimeInterface
    {
        return $this->date_delivery_expected;
    }

    public function setDateDeliveryExpected(\DateTimeInterface $date_delivery_expected): self
    {
        $this->date_delivery_expected = $date_delivery_expected;

        return $this;
    }

    public function getTotalTe(): ?string
    {
        return $this->total_te;
    }

    public function setTotalTe(string $total_te): self
    {
        $this->total_te = $total_te;

        return $this;
    }

    public function getTotalWithDiscountTe(): ?string
    {
        return $this->total_with_discount_te;
    }

    public function setTotalWithDiscountTe(string $total_with_discount_te): self
    {
        $this->total_with_discount_te = $total_with_discount_te;

        return $this;
    }

    public function getTotalTi(): ?string
    {
        return $this->total_ti;
    }

    public function setTotalTi(string $total_ti): self
    {
        $this->total_ti = $total_ti;

        return $this;
    }

    public function getTotalTax(): ?string
    {
        return $this->total_tax;
    }

    public function setTotalTax(string $total_tax): self
    {
        $this->total_tax = $total_tax;

        return $this;
    }

    public function getDiscountRate(): ?string
    {
        return $this->discount_rate;
    }

    public function setDiscountRate(string $discount_rate): self
    {
        $this->discount_rate = $discount_rate;

        return $this;
    }

    public function getDiscountValueTe(): ?string
    {
        return $this->discount_value_te;
    }

    public function setDiscountValueTe(string $discount_value_te): self
    {
        $this->discount_value_te = $discount_value_te;

        return $this;
    }

    public function getIsTemplate(): ?bool
    {
        return $this->is_template;
    }

    public function setIsTemplate(bool $is_template): self
    {
        $this->is_template = $is_template;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->date_add;
    }

    public function setDateAdd(?\DateTimeInterface $date_add): self
    {
        $this->date_add = $date_add;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->date_upd;
    }

    public function setDateUpd(?\DateTimeInterface $date_upd): self
    {
        $this->date_upd = $date_upd;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->Supplier;
    }

    public function setSupplier(?Supplier $Supplier): self
    {
        $this->Supplier = $Supplier;
        $this->setSupplierName($Supplier->getName());
        $this->setIdSupplier($Supplier->getIdSupplier());

        return $this;
    }

    /**
     * @return Collection|SupplierOrderDetails[]
     */
    public function getSupplierOrderDetails(): Collection
    {
        return $this->supplierOrderDetails;
    }

    public function addSupplierOrderDetail(SupplierOrderDetails $supplierOrderDetail): self
    {
        if (!$this->supplierOrderDetails->contains($supplierOrderDetail)) {
            $this->supplierOrderDetails[] = $supplierOrderDetail;
            $supplierOrderDetail->setSupplyOrder($this);
        }

        return $this;
    }

    public function removeSupplierOrderDetail(SupplierOrderDetails $supplierOrderDetail): self
    {
        if ($this->supplierOrderDetails->contains($supplierOrderDetail)) {
            $this->supplierOrderDetails->removeElement($supplierOrderDetail);
            // set the owning side to null (unless already changed)
            if ($supplierOrderDetail->getSupplyOrder() === $this) {
                $supplierOrderDetail->setSupplyOrder(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getReference();
    }

}
