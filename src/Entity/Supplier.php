<?php

namespace App\Entity;

use App\Repository\SupplierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SupplierRepository::class)
 */
class Supplier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link_rewrite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $meta_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $meta_description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $meta_keywords;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_supplier;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="supplier")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=SupplierOrder::class, mappedBy="Supplier")
     */
    private $supplierOrders;

    /**
     * @ORM\OneToMany(targetEntity=PrestashopOrder::class, mappedBy="supplier")
     */
    private $prestashopOrders;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cronTime;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAutoCron = false;

    /**
     * @ORM\OneToMany(targetEntity=OfflineProduct::class, mappedBy="supplier")
     */
    private $offlineProducts;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $schedule = [
        'mon' => null,
        'tue' => null,
        'wed' => null,
        'thu' => null,
        'fri' => null,
        'sat' => null,
        'sun' => null,
    ];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $real_name;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->supplierOrders = new ArrayCollection();
        $this->prestashopOrders = new ArrayCollection();
        $this->offlineProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLinkRewrite(): ?string
    {
        return $this->link_rewrite;
    }

    public function setLinkRewrite(string $link_rewrite): self
    {
        $this->link_rewrite = $link_rewrite;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->meta_title;
    }

    public function setMetaTitle(string $meta_title): self
    {
        $this->meta_title = $meta_title;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): self
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    public function setMetaKeywords(?string $meta_keywords): self
    {
        $this->meta_keywords = $meta_keywords;

        return $this;
    }

    public function getIdSupplier(): ?int
    {
        return $this->id_supplier;
    }

    public function setIdSupplier(int $id_supplier): self
    {
        $this->id_supplier = $id_supplier;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setSupplier($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getSupplier() === $this) {
                $product->setSupplier(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|SupplierOrder[]
     */
    public function getSupplierOrders(): Collection
    {
        return $this->supplierOrders;
    }

    public function addSupplierOrder(SupplierOrder $supplierOrder): self
    {
        if (!$this->supplierOrders->contains($supplierOrder)) {
            $this->supplierOrders[] = $supplierOrder;
            $supplierOrder->setSupplier($this);
        }

        return $this;
    }

    public function removeSupplierOrder(SupplierOrder $supplierOrder): self
    {
        if ($this->supplierOrders->contains($supplierOrder)) {
            $this->supplierOrders->removeElement($supplierOrder);
            // set the owning side to null (unless already changed)
            if ($supplierOrder->getSupplier() === $this) {
                $supplierOrder->setSupplier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PrestashopOrder[]
     */
    public function getPrestashopOrders(): Collection
    {
        return $this->prestashopOrders;
    }

    public function addPrestashopOrder(PrestashopOrder $prestashopOrder): self
    {
        if (!$this->prestashopOrders->contains($prestashopOrder)) {
            $this->prestashopOrders[] = $prestashopOrder;
            $prestashopOrder->setSupplier($this);
        }

        return $this;
    }

    public function removePrestashopOrder(PrestashopOrder $prestashopOrder): self
    {
        if ($this->prestashopOrders->contains($prestashopOrder)) {
            $this->prestashopOrders->removeElement($prestashopOrder);
            // set the owning side to null (unless already changed)
            if ($prestashopOrder->getSupplier() === $this) {
                $prestashopOrder->setSupplier(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCronTime(): ?string
    {
        return $this->cronTime;
    }

    public function setCronTime(?string $cronTime): self
    {
        $this->cronTime = $cronTime;

        return $this;
    }

    public function getIsAutoCron(): ?bool
    {
        return $this->isAutoCron;
    }

    public function setIsAutoCron(bool $isAutoCron): self
    {
        $this->isAutoCron = $isAutoCron;

        return $this;
    }

    /**
     * @return Collection|OfflineProduct[]
     */
    public function getOfflineProducts(): Collection
    {
        return $this->offlineProducts;
    }

    public function addOfflineProduct(OfflineProduct $offlineProduct): self
    {
        if (!$this->offlineProducts->contains($offlineProduct)) {
            $this->offlineProducts[] = $offlineProduct;
            $offlineProduct->setSupplier($this);
        }

        return $this;
    }

    public function removeOfflineProduct(OfflineProduct $offlineProduct): self
    {
        if ($this->offlineProducts->contains($offlineProduct)) {
            $this->offlineProducts->removeElement($offlineProduct);
            // set the owning side to null (unless already changed)
            if ($offlineProduct->getSupplier() === $this) {
                $offlineProduct->setSupplier(null);
            }
        }

        return $this;
    }

    public function getSchedule(): ?array
    {
        return $this->schedule;
    }

    public function setSchedule(?array $schedule): self
    {
        $this->schedule = [
            'mon' => isset($schedule['mon']) ? $schedule['mon'] : null,
            'tue' => isset($schedule['tue']) ? $schedule['tue'] : null,
            'wed' => isset($schedule['wed']) ? $schedule['wed'] : null,
            'thu' => isset($schedule['thu']) ? $schedule['thu'] : null,
            'fri' => isset($schedule['fri']) ? $schedule['fri'] : null,
            'sat' => isset($schedule['sat']) ? $schedule['sat'] : null,
            'sun' => isset($schedule['sun']) ? $schedule['sun'] : null,
        ];
        return $this;
    }

    public function getRealName(): ?string
    {
        return $this->real_name;
    }

    public function setRealName(?string $real_name): self
    {
        $this->real_name = $real_name;

        return $this;
    }
}
