<?php

namespace App\Entity;

use App\Repository\CarrierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarrierRepository::class)
 */
class Carrier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_carrier;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_reference;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_tax_rules_group;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_free;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_width;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_height;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_depth;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_weight;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="carrier")
     */
    private $carts;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $delay;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="carrier")
     */
    private $orders;

    public function __construct()
    {
        $this->carts = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCarrier(): ?int
    {
        return $this->id_carrier;
    }

    public function setIdCarrier(int $id_carrier): self
    {
        $this->id_carrier = $id_carrier;

        return $this;
    }

    public function getIdReference(): ?int
    {
        return $this->id_reference;
    }

    public function setIdReference(int $id_reference): self
    {
        $this->id_reference = $id_reference;

        return $this;
    }

    public function getIdTaxRulesGroup(): ?int
    {
        return $this->id_tax_rules_group;
    }

    public function setIdTaxRulesGroup(int $id_tax_rules_group): self
    {
        $this->id_tax_rules_group = $id_tax_rules_group;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getIsFree(): ?bool
    {
        return $this->is_free;
    }

    public function setIsFree(bool $is_free): self
    {
        $this->is_free = $is_free;

        return $this;
    }

    public function getMaxWidth(): ?int
    {
        return $this->max_width;
    }

    public function setMaxWidth(int $max_width): self
    {
        $this->max_width = $max_width;

        return $this;
    }

    public function getMaxHeight(): ?int
    {
        return $this->max_height;
    }

    public function setMaxHeight(int $max_height): self
    {
        $this->max_height = $max_height;

        return $this;
    }

    public function getMaxDepth(): ?int
    {
        return $this->max_depth;
    }

    public function setMaxDepth(int $max_depth): self
    {
        $this->max_depth = $max_depth;

        return $this;
    }

    public function getMaxWeight(): ?int
    {
        return $this->max_weight;
    }

    public function setMaxWeight(int $max_weight): self
    {
        $this->max_weight = $max_weight;

        return $this;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCart(Cart $cart): self
    {
        if (!$this->carts->contains($cart)) {
            $this->carts[] = $cart;
            $cart->setCarrier($this);
        }

        return $this;
    }

    public function removeCart(Cart $cart): self
    {
        if ($this->carts->contains($cart)) {
            $this->carts->removeElement($cart);
            // set the owning side to null (unless already changed)
            if ($cart->getCarrier() === $this) {
                $cart->setCarrier(null);
            }
        }

        return $this;
    }

    public function getDelay(): ?string
    {
        return $this->delay;
    }

    public function setDelay(string $delay): self
    {
        $this->delay = $delay;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCarrier($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getCarrier() === $this) {
                $order->setCarrier(null);
            }
        }

        return $this;
    }
}
