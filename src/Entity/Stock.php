<?php

namespace App\Entity;

use App\Repository\StockRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_product;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_product_attribute;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_shop;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_shop_group;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $depends_on_stock;

    /**
     * @ORM\Column(type="integer")
     */
    private $out_of_stock;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="stocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class, inversedBy="stocks")
     * @ORM\JoinColumn(nullable=true)
     */
    private $product_attribute;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStockId(): ?int
    {
        return $this->stock_id;
    }

    public function setStockId(int $stock_id): self
    {
        $this->stock_id = $stock_id;

        return $this;
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getIdProductAttribute(): ?int
    {
        return $this->id_product_attribute;
    }

    public function setIdProductAttribute(int $id_product_attribute): self
    {
        $this->id_product_attribute = $id_product_attribute;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return $this->id_shop;
    }

    public function setIdShop(int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getIdShopGroup(): ?int
    {
        return $this->id_shop_group;
    }

    public function setIdShopGroup(int $id_shop_group): self
    {
        $this->id_shop_group = $id_shop_group;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDependsOnStock(): ?int
    {
        return $this->depends_on_stock;
    }

    public function setDependsOnStock(int $depends_on_stock): self
    {
        $this->depends_on_stock = $depends_on_stock;

        return $this;
    }

    public function getOutOfStock(): ?int
    {
        return $this->out_of_stock;
    }

    public function setOutOfStock(int $out_of_stock): self
    {
        $this->out_of_stock = $out_of_stock;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductAttribute(): ?ProductAttribute
    {
        return $this->product_attribute;
    }

    public function setProductAttribute(?ProductAttribute $product_attribute): self
    {
        $this->product_attribute = $product_attribute;

        return $this;
    }


    public function __toString()
    {
        return (string)$this->quantity;
    }
}
