<?php

namespace App\Entity;

use App\Repository\PrestashopOrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrestashopOrderRepository::class)
 */
class PrestashopOrder
{
    const STATUS_IN_PROGRESS = 0;
    const STATUS_ORDERED = 1;
    const STATUS_VALIDATED = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\OneToMany(targetEntity=PrestashopOrderDetail::class, mappedBy="prestashopOrder", orphanRemoval=true)
     */
    private $prestashopOrderDetails;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="prestashopOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $supplier;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = 0;

    /**
     * @ORM\ManyToOne(targetEntity=CompanyAddress::class, inversedBy="prestashop_orders")
     */
    private $companyAddress;

    /**
     * @ORM\ManyToOne(targetEntity=CompanyAddress::class, inversedBy="invoice_prestashop_orders")
     */
    private $invoice_address;


    public function __construct()
    {
        $this->prestashopOrderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPrice(): ?string
    {
        $this->price = 0;
        $this->getPrestashopOrderDetails()->map(function (PrestashopOrderDetail $detail) {
            $this->price += $detail->getPrice();
        });
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }


    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Collection|PrestashopOrderDetail[]
     */
    public function getPrestashopOrderDetails(): Collection
    {
        return $this->prestashopOrderDetails;
    }

    public function addPrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if (!$this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails[] = $prestashopOrderDetail;
            $prestashopOrderDetail->setPrestashopOrder($this);
        }

        return $this;
    }

    public function removePrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if ($this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails->removeElement($prestashopOrderDetail);
            // set the owning side to null (unless already changed)
            if ($prestashopOrderDetail->getPrestashopOrder() === $this) {
                $prestashopOrderDetail->setPrestashopOrder(null);
            }
        }

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->reference;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function getStatusName()
    {
        switch ($this->status) {
            case self::STATUS_IN_PROGRESS:
                return "in progress";
            case self::STATUS_ORDERED:
                return "ordered";
            case self::STATUS_VALIDATED:
                return "validated";
        }
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProductCount()
    {
        $count = 0;
        foreach ($this->getPrestashopOrderDetails() as $detail)
            $count += $detail->getQuantity();
        return $count;
    }

    public function getCompanyAddress(): ?CompanyAddress
    {
        return $this->companyAddress;
    }

    public function setCompanyAddress(?CompanyAddress $companyAddress): self
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    public function getInvoiceAddress(): ?CompanyAddress
    {
        return $this->invoice_address;
    }

    public function setInvoiceAddress(?CompanyAddress $invoice_address): self
    {
        $this->invoice_address = $invoice_address;

        return $this;
    }
}
