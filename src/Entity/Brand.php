<?php

namespace App\Entity;

use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 */
class Brand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_add;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_upd;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $short_description;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id_manufacturer;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="manufacturer")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=OfflineProduct::class, mappedBy="manufacturer")
     */
    private $offlineProducts;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->offlineProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->date_add;
    }

    public function setDateAdd(\DateTimeInterface $date_add): self
    {
        $this->date_add = $date_add;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->date_upd;
    }

    public function setDateUpd(\DateTimeInterface $date_upd): self
    {
        $this->date_upd = $date_upd;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->short_description;
    }

    public function setShortDescription(?string $short_description): self
    {
        $this->short_description = $short_description;

        return $this;
    }

    public function getIdManufacturer(): ?int
    {
        return $this->id_manufacturer;
    }

    public function setIdManufacturer(int $id_manufacturer): self
    {
        $this->id_manufacturer = $id_manufacturer;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setManufacturer($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getManufacturer() === $this) {
                $product->setManufacturer(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * @return Collection|OfflineProduct[]
     */
    public function getOfflineProducts(): Collection
    {
        return $this->offlineProducts;
    }

    public function addOfflineProduct(OfflineProduct $offlineProduct): self
    {
        if (!$this->offlineProducts->contains($offlineProduct)) {
            $this->offlineProducts[] = $offlineProduct;
            $offlineProduct->setManufacturer($this);
        }

        return $this;
    }

    public function removeOfflineProduct(OfflineProduct $offlineProduct): self
    {
        if ($this->offlineProducts->contains($offlineProduct)) {
            $this->offlineProducts->removeElement($offlineProduct);
            // set the owning side to null (unless already changed)
            if ($offlineProduct->getManufacturer() === $this) {
                $offlineProduct->setManufacturer(null);
            }
        }

        return $this;
    }
}
