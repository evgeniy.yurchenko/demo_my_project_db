<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_category;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_parent;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_shop_default;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_root_category;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="categories")
     */
    private $products;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=OfflineProduct::class, mappedBy="categories")
     */
    private $offlineProducts;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->offlineProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCategory(): ?int
    {
        return $this->id_category;
    }

    public function setIdCategory(int $id_category): self
    {
        $this->id_category = $id_category;

        return $this;
    }

    public function getIdParent(): ?int
    {
        return $this->id_parent;
    }

    public function setIdParent(int $id_parent): self
    {
        $this->id_parent = $id_parent;

        return $this;
    }

    public function getIdShopDefault(): ?int
    {
        return $this->id_shop_default;
    }

    public function setIdShopDefault(int $id_shop_default): self
    {
        $this->id_shop_default = $id_shop_default;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getIsRootCategory(): ?bool
    {
        return $this->is_root_category;
    }

    public function setIsRootCategory(bool $is_root_category): self
    {
        $this->is_root_category = $is_root_category;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|OfflineProduct[]
     */
    public function getOfflineProducts(): Collection
    {
        return $this->offlineProducts;
    }

    public function addOfflineProduct(OfflineProduct $offlineProduct): self
    {
        if (!$this->offlineProducts->contains($offlineProduct)) {
            $this->offlineProducts[] = $offlineProduct;
            $offlineProduct->addCategory($this);
        }

        return $this;
    }

    public function removeOfflineProduct(OfflineProduct $offlineProduct): self
    {
        if ($this->offlineProducts->contains($offlineProduct)) {
            $this->offlineProducts->removeElement($offlineProduct);
            $offlineProduct->removeCategory($this);
        }

        return $this;
    }
}
