<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_order;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_cart;

    /**
     * @ORM\ManyToOne(targetEntity=Cart::class, inversedBy="orders")
     * @ORM\JoinColumn()
     */
    private $cart;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_currency;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_address_delivery;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="orders_delivery")
     * @ORM\JoinColumn(nullable=true)
     */
    private $address_delivery;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_address_invoice;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="orders_invoice")
     * @ORM\JoinColumn(nullable=true)
     */
    private $address_invoice;

    /**
     * @ORM\Column(type="integer")
     */
    private $current_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_customer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secure_key;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $payment;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=6)
     */
    private $conversion_rate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $recyclable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gift;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shipping_number;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $total_discounts;

    /**
     * @ORM\ManyToOne(targetEntity=OrderState::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_carrier;

    /**
     * @ORM\ManyToOne(targetEntity=Carrier::class, inversedBy="orders")
     */
    private $carrier;

    /**
     * @ORM\OneToMany(targetEntity=OrderProduct::class, mappedBy="orderConnected", orphanRemoval=true)
     */
    private $orderProducts;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6, nullable=true)
     */
    private $order_weight;

    /*
     * @ORM\OneToMany(targetEntity=OrderDetails::class, mappedBy="connected_order")
     */
    private $orderDetails;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_add;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6, nullable=true)
     */
    private $total_paid_tax_incl;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6, nullable=true)
     */
    private $total_paid_tax_excl;

    private $isForPreparation;

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->orderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdOrder(): ?int
    {
        return $this->id_order;
    }

    public function setIdOrder(int $id_order): self
    {
        $this->id_order = $id_order;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

//    public function getIdShopGroup(): ?int
//    {
//        return $this->id_shop_group;
//    }
//
//    public function setIdShopGroup(int $id_shop_group): self
//    {
//        $this->id_shop_group = $id_shop_group;
//
//        return $this;
//    }
//
//    public function getIdShop(): ?int
//    {
//        return $this->id_shop;
//    }
//
//    public function setIdShop(int $id_shop): self
//    {
//        $this->id_shop = $id_shop;
//
//        return $this;
//    }
//
//    public function getIdLang(): ?int
//    {
//        return $this->id_lang;
//    }
//
//    public function setIdLang(int $id_lang): self
//    {
//        $this->id_lang = $id_lang;
//
//        return $this;
//    }

    public function getIdCustomer(): ?int
    {
        return $this->id_customer;
    }

    public function setIdCustomer(int $id_customer): self
    {
        $this->id_customer = $id_customer;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getIdCart(): ?int
    {
        return $this->id_cart;
    }

    public function setIdCart(int $id_cart): self
    {
        $this->id_cart = $id_cart;

        return $this;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getIdCurrency(): ?int
    {
        return $this->id_currency;
    }

    public function setIdCurrency(int $id_currency): self
    {
        $this->id_currency = $id_currency;

        return $this;
    }

    public function getIdAddressDelivery(): ?int
    {
        return $this->id_address_delivery;
    }

    public function setIdAddressDelivery(int $id_address_delivery): self
    {
        $this->id_address_delivery = $id_address_delivery;

        return $this;
    }

    public function getAddressDelivery(): ?Address
    {
        return $this->address_delivery;
    }

    public function setAddressDelivery(?Address $address_delivery): self
    {
        $this->address_delivery = $address_delivery;

        return $this;
    }

    public function getIdAddressInvoice(): ?int
    {
        return $this->id_address_invoice;
    }

    public function setIdAddressInvoice(int $id_address_invoice): self
    {
        $this->id_address_invoice = $id_address_invoice;

        return $this;
    }

    public function getAddressInvoice(): ?Address
    {
        return $this->address_invoice;
    }

    public function setAddressInvoice(?Address $address_invoice): self
    {
        $this->address_invoice = $address_invoice;

        return $this;
    }

    public function getCurrentState(): ?int
    {
        return $this->current_state;
    }

    public function setCurrentState(int $current_state): self
    {
        $this->current_state = $current_state;

        return $this;
    }

    public function getSecureKey(): ?string
    {
        return $this->secure_key;
    }

    public function setSecureKey(string $secure_key): self
    {
        $this->secure_key = $secure_key;

        return $this;
    }

    public function getPayment(): ?string
    {
        return $this->payment;
    }

    public function setPayment(string $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getConversionRate(): ?string
    {
        return $this->conversion_rate;
    }

    public function setConversionRate(string $conversion_rate): self
    {
        $this->conversion_rate = $conversion_rate;

        return $this;
    }

    public function getRecyclable(): ?bool
    {
        return $this->recyclable;
    }

    public function setRecyclable(bool $recyclable): self
    {
        $this->recyclable = $recyclable;

        return $this;
    }

    public function getGift(): ?bool
    {
        return $this->gift;
    }

    public function setGift(bool $gift): self
    {
        $this->gift = $gift;

        return $this;
    }

    public function getShippingNumber(): ?string
    {
        return $this->shipping_number;
    }

    public function setShippingNumber(string $shipping_number): self
    {
        $this->shipping_number = $shipping_number;

        return $this;
    }

    public function getTotalDiscounts(): ?string
    {
        return $this->total_discounts;
    }

    public function setTotalDiscounts(string $total_discounts): self
    {
        $this->total_discounts = $total_discounts;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getCartProducts(): ?Collection
    {
        if ($this->getCart())
            return $this->getCart()->getCartProducts();
        return null;
    }

    public function __toString()
    {
        return $this->getReference();
    }

    public function getState(): ?OrderState
    {
        return $this->state;
    }

    public function setState(?OrderState $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getIdCarrier(): ?int
    {
        return $this->id_carrier;
    }

    public function setIdCarrier(int $id_carrier): self
    {
        $this->id_carrier = $id_carrier;

        return $this;
    }

    public function getCarrier(): ?Carrier
    {
        return $this->carrier;
    }

    public function setCarrier(?Carrier $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * @return Collection|OrderProduct[]
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        if (!$this->orderProducts->contains($orderProduct)) {
            $this->orderProducts[] = $orderProduct;
            $orderProduct->setOrderConnected($this);
        }

        return $this;
    }

    public function removeOrderProduct(OrderProduct $orderProduct): self
    {
        if ($this->orderProducts->contains($orderProduct)) {
            $this->orderProducts->removeElement($orderProduct);
            // set the owning side to null (unless already changed)
            if ($orderProduct->getOrderConnected() === $this) {
                $orderProduct->setOrderConnected(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|OrderDetails[]
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function addOrderDetail(OrderDetails $orderDetail): self
    {
        if (!$this->orderDetails->contains($orderDetail)) {
            $this->orderDetails[] = $orderDetail;
            $orderDetail->setConnectedOrder($this);
        }

        return $this;
    }

    public function removeOrderDetail(OrderDetails $orderDetail): self
    {
        if ($this->orderDetails->contains($orderDetail)) {
            $this->orderDetails->removeElement($orderDetail);
            // set the owning side to null (unless already changed)
            if ($orderDetail->getConnectedOrder() === $this) {
                $orderDetail->setConnectedOrder(null);
            }
        }

        return $this;
    }

    public function getOrderWeight(): ?string
    {
        return $this->order_weight;
    }

    public function setOrderWeight(?string $order_weight): self
    {
        $this->order_weight = $order_weight;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->date_add;
    }

    public function setDateAdd(?\DateTimeInterface $date_add): self
    {
        $this->date_add = $date_add;

        return $this;
    }

    public function getTotalPaidTaxIncl(): ?string
    {
        return $this->total_paid_tax_incl;
    }

    public function setTotalPaidTaxIncl(?string $total_paid_tax_incl): self
    {
        $this->total_paid_tax_incl = $total_paid_tax_incl;

        return $this;
    }

    public function getTotalPaidTaxExcl(): ?string
    {
        return $this->total_paid_tax_excl;
    }

    public function setTotalPaidTaxExcl(?string $total_paid_tax_excl): self
    {
        $this->total_paid_tax_excl = $total_paid_tax_excl;

        return $this;
    }

    public function getColor()
    {
        $flag = true;
        foreach ($this->getOrderProducts() as $orderProduct) {
            $stock = $orderProduct->getProduct()->getAvailableStock($orderProduct->getProductAttribute());
            if ($stock < 0) {
                $flag = false;
                break;
            }
        }
        if ($flag) {
            return "#78e08f";
        }
        $dateAdd = $this->getDateAdd();
        $now = new \DateTime();
        $dateInterval = date_diff($dateAdd, $now)->format('%a');
        if ($dateInterval == 4) {
            return '#eb4d4b';
        }
        if ($dateInterval > 5) {
            return '#eb2f06';
        }
        return null;
    }

    public function getIsForPreparation(): ?bool
    {
        return $this->getState()->getIsForPreparation();
    }

    public function setIsForPreparation(bool $isForPreparation): self
    {
        $this->isForPreparation = $isForPreparation;

        return $this;
    }
}
