<?php

namespace App\Entity;

use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddressRepository::class)
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_address;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_country;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_state;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="addresses")
     * @ORM\JoinColumn(nullable=true)
     */
    private $customer;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_manufacturer;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_supplier;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_warehouse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $other;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_mobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vat_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dni;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deleted;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="address_delivery")
     */
    private $orders_delivery;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="address_invoice")
     */
    private $orders_invoice;

    /**
     * @ORM\OneToMany(targetEntity=CartProduct::class, mappedBy="address_delivery")
     */
    private $cartProducts;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_customer;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="address_delivery")
     */
    private $carts_delivery;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="address_invoice")
     */
    private $carts_invoice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    public function __construct()
    {
        $this->orders_delivery = new ArrayCollection();
        $this->orders_invoice = new ArrayCollection();
        $this->cartProducts = new ArrayCollection();
        $this->carts_delivery = new ArrayCollection();
        $this->carts_invoice = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdAddress(): ?int
    {
        return $this->id_address;
    }

    public function setIdAddress(int $id_address): self
    {
        $this->id_address = $id_address;

        return $this;
    }

    public function getIdCountry(): ?int
    {
        return $this->id_country;
    }

    public function setIdCountry(int $id_country): self
    {
        $this->id_country = $id_country;

        return $this;
    }

    public function getIdState(): ?int
    {
        return $this->id_state;
    }

    public function setIdState(int $id_state): self
    {
        $this->id_state = $id_state;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getIdManufacturer(): ?int
    {
        return $this->id_manufacturer;
    }

    public function setIdManufacturer(int $id_manufacturer): self
    {
        $this->id_manufacturer = $id_manufacturer;

        return $this;
    }

    public function getIdSupplier(): ?int
    {
        return $this->id_supplier;
    }

    public function setIdSupplier(int $id_supplier): self
    {
        $this->id_supplier = $id_supplier;

        return $this;
    }

    public function getIdWarehouse(): ?int
    {
        return $this->id_warehouse;
    }

    public function setIdWarehouse(int $id_warehouse): self
    {
        $this->id_warehouse = $id_warehouse;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(?string $other): self
    {
        $this->other = $other;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneMobile(): ?string
    {
        return $this->phone_mobile;
    }

    public function setPhoneMobile(?string $phone_mobile): self
    {
        $this->phone_mobile = $phone_mobile;

        return $this;
    }

    public function getVatNumber(): ?string
    {
        return $this->vat_number;
    }

    public function setVatNumber(?string $vat_number): self
    {
        $this->vat_number = $vat_number;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(?string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrdersDelivery(): Collection
    {
        return $this->orders_delivery;
    }

    public function addOrdersDelivery(Order $ordersDelivery): self
    {
        if (!$this->orders_delivery->contains($ordersDelivery)) {
            $this->orders_delivery[] = $ordersDelivery;
            $ordersDelivery->setAddressDelivery($this);
        }

        return $this;
    }

    public function removeOrdersDelivery(Order $ordersDelivery): self
    {
        if ($this->orders_delivery->contains($ordersDelivery)) {
            $this->orders_delivery->removeElement($ordersDelivery);
            // set the owning side to null (unless already changed)
            if ($ordersDelivery->getAddressDelivery() === $this) {
                $ordersDelivery->setAddressDelivery(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrdersInvoice(): Collection
    {
        return $this->orders_invoice;
    }

    public function addOrdersInvoice(Order $ordersInvoice): self
    {
        if (!$this->orders_invoice->contains($ordersInvoice)) {
            $this->orders_invoice[] = $ordersInvoice;
            $ordersInvoice->setAddressInvoice($this);
        }

        return $this;
    }

    public function removeOrdersInvoice(Order $ordersInvoice): self
    {
        if ($this->orders_invoice->contains($ordersInvoice)) {
            $this->orders_invoice->removeElement($ordersInvoice);
            // set the owning side to null (unless already changed)
            if ($ordersInvoice->getAddressInvoice() === $this) {
                $ordersInvoice->setAddressInvoice(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CartProduct[]
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): self
    {
        if (!$this->cartProducts->contains($cartProduct)) {
            $this->cartProducts[] = $cartProduct;
            $cartProduct->setAddressDelivery($this);
        }

        return $this;
    }

    public function removeCartProduct(CartProduct $cartProduct): self
    {
        if ($this->cartProducts->contains($cartProduct)) {
            $this->cartProducts->removeElement($cartProduct);
            // set the owning side to null (unless already changed)
            if ($cartProduct->getAddressDelivery() === $this) {
                $cartProduct->setAddressDelivery(null);
            }
        }

        return $this;
    }

    public function getIdCustomer(): ?int
    {
        return $this->id_customer;
    }

    public function setIdCustomer(int $id_customer): self
    {
        $this->id_customer = $id_customer;

        return $this;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCartsDelivery(): Collection
    {
        return $this->carts_delivery;
    }

    public function addCartsDelivery(Cart $cartsDelivery): self
    {
        if (!$this->carts_delivery->contains($cartsDelivery)) {
            $this->carts_delivery[] = $cartsDelivery;
            $cartsDelivery->setAddressDelivery($this);
        }

        return $this;
    }

    public function removeCartsDelivery(Cart $cartsDelivery): self
    {
        if ($this->carts_delivery->contains($cartsDelivery)) {
            $this->carts_delivery->removeElement($cartsDelivery);
            // set the owning side to null (unless already changed)
            if ($cartsDelivery->getAddressDelivery() === $this) {
                $cartsDelivery->setAddressDelivery(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCartsInvoice(): Collection
    {
        return $this->carts_invoice;
    }

    public function addCartsInvoice(Cart $cartsInvoice): self
    {
        if (!$this->carts_invoice->contains($cartsInvoice)) {
            $this->carts_invoice[] = $cartsInvoice;
            $cartsInvoice->setAddressInvoice($this);
        }

        return $this;
    }

    public function removeCartsInvoice(Cart $cartsInvoice): self
    {
        if ($this->carts_invoice->contains($cartsInvoice)) {
            $this->carts_invoice->removeElement($cartsInvoice);
            // set the owning side to null (unless already changed)
            if ($cartsInvoice->getAddressInvoice() === $this) {
                $cartsInvoice->setAddressInvoice(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->firstname;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }
}
