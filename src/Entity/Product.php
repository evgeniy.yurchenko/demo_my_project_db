<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_product;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_supplier;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_category_default;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_tax_rules_group;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ean13;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $upc;

    /**
     * @ORM\Column(type="decimal", precision=23, scale=6, nullable=true)
     */
    private $ecotax;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $minimal_quantity;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $wholesale_price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $supplier_reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $width;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $height;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $depth;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $weight;

    /**
     * @ORM\Column(type="integer")
     */
    private $out_of_stock;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="products")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity=CartProduct::class, mappedBy="product")
     */
    private $cartProducts;

    /**
     * @ORM\OneToMany(targetEntity=ProductAttribute::class, mappedBy="product")
     */
    private $productAttributes;

    /**
     * @ORM\OneToMany(targetEntity=OrderProduct::class, mappedBy="product")
     */
    private $orderProducts;

    /**
     * @ORM\OneToMany(targetEntity=OrderDetails::class, mappedBy="connected_product")
     */
    private $orderDetails;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;


    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="product")
     */
    private $stocks;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="products")
     */
    private $supplier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=SupplierOrderDetails::class, mappedBy="product")
     */
    private $supplierOrderDetails;

    /**
     * @ORM\OneToMany(targetEntity=PrestashopOrderDetail::class, mappedBy="product")
     */
    private $prestashopOrderDetails;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="products")
     */
    private $manufacturer;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->cartProducts = new ArrayCollection();
        $this->productAttributes = new ArrayCollection();
        $this->orderProducts = new ArrayCollection();
        $this->orderDetails = new ArrayCollection();
        $this->stocks = new ArrayCollection();
        $this->supplierOrderDetails = new ArrayCollection();
        $this->prestashopOrderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getIdSupplier(): ?int
    {
        return $this->id_supplier;
    }

    public function setIdSupplier(int $id_supplier): self
    {
        $this->id_supplier = $id_supplier;

        return $this;
    }

    public function getIdCategoryDefault(): ?int
    {
        return $this->id_category_default;
    }

    public function setIdCategoryDefault(int $id_category_default): self
    {
        $this->id_category_default = $id_category_default;

        return $this;
    }

    public function getIdTaxRulesGroup(): ?int
    {
        return $this->id_tax_rules_group;
    }

    public function setIdTaxRulesGroup(int $id_tax_rules_group): self
    {
        $this->id_tax_rules_group = $id_tax_rules_group;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getUpc(): ?string
    {
        return $this->upc;
    }

    public function setUpc(string $upc): self
    {
        $this->upc = $upc;

        return $this;
    }

    public function getEcotax(): ?string
    {
        return $this->ecotax;
    }

    public function setEcotax(?string $ecotax): self
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getMinimalQuantity(): ?int
    {
        return $this->minimal_quantity;
    }

    public function setMinimalQuantity(int $minimal_quantity): self
    {
        $this->minimal_quantity = $minimal_quantity;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getWholesalePrice(): ?string
    {
        return $this->wholesale_price;
    }

    public function setWholesalePrice(string $wholesale_price): self
    {
        $this->wholesale_price = $wholesale_price;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSupplierReference(): ?string
    {
        return $this->supplier_reference;
    }

    public function setSupplierReference(string $supplier_reference): self
    {
        $this->supplier_reference = $supplier_reference;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getDepth(): ?string
    {
        return $this->depth;
    }

    public function setDepth(string $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getOutOfStock(): ?int
    {
        return $this->out_of_stock;
    }

    public function setOutOfStock(int $out_of_stock): self
    {
        $this->out_of_stock = $out_of_stock;

        return $this;
    }


    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addProduct($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|CartProduct[]
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): self
    {
        if (!$this->cartProducts->contains($cartProduct)) {
            $this->cartProducts[] = $cartProduct;
            $cartProduct->setProduct($this);
        }

        return $this;
    }

    public function removeCartProduct(CartProduct $cartProduct): self
    {
        if ($this->cartProducts->contains($cartProduct)) {
            $this->cartProducts->removeElement($cartProduct);
            // set the owning side to null (unless already changed)
            if ($cartProduct->getProduct() === $this) {
                $cartProduct->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductAttribute[]
     */
    public function getProductAttributes(): Collection
    {
        return $this->productAttributes;
    }

    public function addProductAttribute(ProductAttribute $productAttribute): self
    {
        if (!$this->productAttributes->contains($productAttribute)) {
            $this->productAttributes[] = $productAttribute;
            $productAttribute->setProduct($this);
        }

        return $this;
    }

    public function removeProductAttribute(ProductAttribute $productAttribute): self
    {
        if ($this->productAttributes->contains($productAttribute)) {
            $this->productAttributes->removeElement($productAttribute);
            // set the owning side to null (unless already changed)
            if ($productAttribute->getProduct() === $this) {
                $productAttribute->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderProduct[]
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        if (!$this->orderProducts->contains($orderProduct)) {
            $this->orderProducts[] = $orderProduct;
            $orderProduct->setProduct($this);
        }

        return $this;

    }

    /**
     * @return Collection|OrderDetails[]
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function addOrderDetail(OrderDetails $orderDetail): self
    {
        if (!$this->orderDetails->contains($orderDetail)) {
            $this->orderDetails[] = $orderDetail;
            $orderDetail->setConnectedProduct($this);
        }

        return $this;
    }

    public function removeOrderProduct(OrderProduct $orderProduct): self
    {
        if ($this->orderProducts->contains($orderProduct)) {
            $this->orderProducts->removeElement($orderProduct);
            // set the owning side to null (unless already changed)
            if ($orderProduct->getProduct() === $this) {
                $orderProduct->setProduct(null);
            }
        }
        return $this;

    }


    public function removeOrderDetail(OrderDetails $orderDetail): self
    {
        if ($this->orderDetails->contains($orderDetail)) {
            $this->orderDetails->removeElement($orderDetail);
            // set the owning side to null (unless already changed)
            if ($orderDetail->getConnectedProduct() === $this) {
                $orderDetail->setConnectedProduct(null);
            }
        }

        return $this;
    }


    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }


    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setProduct($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->contains($stock)) {
            $this->stocks->removeElement($stock);
            // set the owning side to null (unless already changed)
            if ($stock->getProduct() === $this) {
                $stock->setProduct(null);
            }
        }

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getAvailableStock(ProductAttribute $productAttribute = null)
    {
        $stocks = $this->getStocks();
        $resultStocks = $stocks->filter(function (Stock $stock) use ($productAttribute) {
            return $stock->getProductAttribute() == $productAttribute;
        });
        return $resultStocks->first() ? $resultStocks->first()->getQuantity() : null;
    }

    public function getStock(ProductAttribute $productAttribute = null)
    {
        $stocks = $this->getStocks();
        $resultStocks = $stocks->filter(function (Stock $stock) use ($productAttribute) {
            return $stock->getProductAttribute() == $productAttribute;
        });
        return $resultStocks->first();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|SupplierOrderDetails[]
     */
    public function getSupplierOrderDetails(): Collection
    {
        return $this->supplierOrderDetails;
    }

    public function addSupplierOrderDetail(SupplierOrderDetails $supplierOrderDetail): self
    {
        if (!$this->supplierOrderDetails->contains($supplierOrderDetail)) {
            $this->supplierOrderDetails[] = $supplierOrderDetail;
            $supplierOrderDetail->setProduct($this);
        }

        return $this;
    }

    public function removeSupplierOrderDetail(SupplierOrderDetails $supplierOrderDetail): self
    {
        if ($this->supplierOrderDetails->contains($supplierOrderDetail)) {
            $this->supplierOrderDetails->removeElement($supplierOrderDetail);
            // set the owning side to null (unless already changed)
            if ($supplierOrderDetail->getProduct() === $this) {
                $supplierOrderDetail->setProduct(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->reference ? $this->reference : $this->getName();
    }

    /**
     * @return Collection|PrestashopOrderDetail[]
     */
    public function getPrestashopOrderDetails(): Collection
    {
        return $this->prestashopOrderDetails;
    }

    public function addPrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if (!$this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails[] = $prestashopOrderDetail;
            $prestashopOrderDetail->setProduct($this);
        }

        return $this;
    }

    public function removePrestashopOrderDetail(PrestashopOrderDetail $prestashopOrderDetail): self
    {
        if ($this->prestashopOrderDetails->contains($prestashopOrderDetail)) {
            $this->prestashopOrderDetails->removeElement($prestashopOrderDetail);
            // set the owning side to null (unless already changed)
            if ($prestashopOrderDetail->getProduct() === $this) {
                $prestashopOrderDetail->setProduct(null);
            }
        }

        return $this;
    }

    public function encodeImageToBase64()
    {
        if (!$this->getImage()) return null;

        try {
            $binImage = file_get_contents($this->getImage());
        } catch (\Exception $exception) {
            return null;
        }
        sleep(1);
        $base64Encoding = base64_encode($binImage);
        return 'data:image/png;base64,' . $base64Encoding;
    }

    public function getManufacturer(): ?Brand
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Brand $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }
}
