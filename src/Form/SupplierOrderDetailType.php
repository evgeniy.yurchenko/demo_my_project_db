<?php

namespace App\Form;

use App\Entity\SupplierOrderDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplierOrderDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('id_supplier_order_details')
//            ->add('id_product')
//            ->add('id_product_attribute')
            ->add('product')
            ->add('product_attribute')
            ->add('quantity');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SupplierOrderDetails::class,
        ]);
    }
}
