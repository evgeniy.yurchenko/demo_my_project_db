<?php

namespace App\Form;

use App\Entity\OfflineProduct;
use App\Entity\PrestashopOrder;
use App\Entity\Supplier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfflineProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ean13', TextType::class)
            ->add('name')
            ->add('isbn')
            ->add('upc')
            ->add('quantity')
            ->add('price')
            ->add('wholesale_price')
            ->add('reference')
            ->add('supplier_reference')
            ->add('weight')
            ->add('supplier', HiddenType::class, [
                'mapped' => false
            ])
            ->add('order', HiddenType::class, [
                'mapped' => false
            ])
            ->add('manufacturer')
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OfflineProduct::class,
        ]);
    }
}
