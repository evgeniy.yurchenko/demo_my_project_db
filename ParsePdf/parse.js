const Driver = require('./driver')
const {By, until} = require('selenium-webdriver');
const fs = require('fs')
const {credentials, target_url,download_path,order_id} = require('./config.json');

exports.parse = async function () {
    let get_params = '&vieworder=&id_order=' + order_id;
    let driver = await Driver.getDriver();
    await driver.get(target_url);
 console.log("--waiting until page load--");
    await driver.wait(until.elementLocated(By.id('email')), 15000);
    await driver.wait(until.elementLocated(By.id('passwd')), 15000);
    console.log('--trying to login--');
    await driver.findElement(By.id('email')).sendKeys(credentials.login);
    await driver.findElement(By.id('passwd')).sendKeys(credentials.password);
    await driver.findElement(By.id('submit_login')).click();
    await driver.wait(until.elementLocated(By.name('orderFilter_id_order')), 15000);
    console.log('--success login--');
    let url = await driver.getCurrentUrl();
    await driver.get(url + get_params);
    await driver.wait(until.elementLocated(By.id('chronoSubmitButton')), 15000);
    console.log('--success order page loaded--');
    console.log('--downloading pdf--');
    await driver.findElement(By.id('chronoSubmitButton')).click();
    driver.quit()
    let files = fs.readdirSync(download_path);
    if (!files.length) {
        console.log("--download doesn't success--");
        return false;
    } else {
        console.log("--download success--");
        return files[0];
    }
}
