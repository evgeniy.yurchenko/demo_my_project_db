const {Builder} = require('selenium-webdriver');
const firefox = require("selenium-webdriver/firefox");
const {download_path} = require('./config.json');
let driver;

async function initialize() {
    const screen = {
        width: 1920,
        height: 1080
    };
    let options = new firefox.Options()
        .setBinary("/home/prestaweb360dev/www/dev-erp/ParsePdf/firefox/firefox")
        .setPreference("browser.helperApps.alwaysAsk.force", false)
        .setPreference("pdfjs.disabled", true)
        .setPreference("browser.download.folderList", 2)
        .setPreference("browser.download.useDownloadDir", true)
        .setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
        .setPreference("browser.download.dir", download_path)
        .headless().windowSize(screen)
    driver = await new Builder().forBrowser('firefox').setFirefoxOptions(options).build();
    return driver;
}

exports.getDriver = async function getDriver() {
    if (!driver) driver = await initialize();
    return driver;
};
